def check_title(project):
    error = ''
    if not ('title' in project.keys()):
        error = 'title missing'
        return error, False
    if not (isinstance(project['title'], str)):
        error = 'title is not a string: ' + str(project['title'])
        return error, False
    if not (len(project['title']) > 0):
        error = 'empty title'
        return error, False
    return error, True