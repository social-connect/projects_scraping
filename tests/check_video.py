def check_video(project):
    error = ''
    if not ('video' in project.keys()):
        error = 'video missing'
        return error, False
    if not(type(project['video']) is list):
        error = 'video is not a list: ' + str(project['video'])
        return error, False
    return error, True