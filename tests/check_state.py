def check_state(project):
    error = ''
    if not ('state' in project.keys()):
        error = 'state missing'
        return error, False
    if not (isinstance(project['state'], str)):
        error = 'state is not a string: ' + str(project['state'])
        return error, False
    return error, True
