def check_link(project):
    error = ''
    if not ('link' in project.keys()):
        error = 'link missing'
        return error, False
    if not (isinstance(project['link'], str)):
        error = 'link is not a string: ' + str(project['link'])
        return error, False
    if not (len(project['link']) > 0):
        error = 'empty link'
        return error, False
    return error, True
