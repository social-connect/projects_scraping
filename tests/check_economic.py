def check_economic(project):
    error = ''
    if not ('economic' in project.keys()):
        error = 'economic missing'
        return error, False
    if not (isinstance(project['economic'], str)):
        error = 'economic is not a string: ' + str(project['economic'])
        return error, False
    return error, True
