def check_img(project):
    error = ''
    if not ('img' in project.keys()):
        error = 'img missing'
        return error, False
    if not (isinstance(project['img'], str)):
        error = 'img is not a string: ' + str(project['img'])
        return error, False
    return error, True