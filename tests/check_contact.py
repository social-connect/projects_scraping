def check_contact(project):
    error = ''
    if not ('contact' in project.keys()):
        error = 'contact missing'
        return error, False
    if not (isinstance(project['contact'], str)):
        error = 'contact is not a string: ' + str(project['contact'])
        return error, False
    return error, True
