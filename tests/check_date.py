def check_date(project):
    error = ''
    if not ('date' in project.keys()):
        error = 'date missing'
        return error, False
    if not (isinstance(project['date'], str)):
        error = 'date is not a string: ' + str(project['date'])
        return error, False
    return error, True
