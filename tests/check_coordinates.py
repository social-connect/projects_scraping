def check_coordinates(project):
    error = ''
    if not ('coordinates' in project.keys()):
        error = 'coordinates missing'
        return error, False
    if not(type(project['coordinates']) is list):
        error = 'coordinates is not a list: ' + str(project['coordinates'])
        return error, False
    return error, True