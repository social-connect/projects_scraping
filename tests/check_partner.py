def check_partner(project):
    error = ''
    if not ('partner' in project.keys()):
        error = 'partner missing'
        return error, False
    if not (type(project['partner']) is list):
        error = 'partner is not a list: ' + str(project['partner'])
        return error, False
    return error, True
