def check_key_words(project):
    error = ''
    if not ('key_words' in project.keys()):
        error = 'key_words missing'
        return error, False
    if not (type(project['key_words']) is list):
        error = 'key_words is not a list: ' + str(project['key_words'])
        return error, False
    return error, True
