def check_project_holder(project):
    error = ''
    if not ('project_holder' in project.keys()):
        error = 'project_holder missing'
        return error, False
    if not (isinstance(project['project_holder'], str)):
        error = 'project_holder is not a string: ' + str(project['project_holder'])
        return error, False
    return error, True
