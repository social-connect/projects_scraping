def check_idx(project):
    error = ''
    if not ('idx' in project.keys()):
        error = 'idx missing'
        return error, False
    if not (isinstance(project['idx'], int)):
        error = 'idx is not a integer: ' + str(project['idx'])
        return error, False
    if not (project['idx'] >= 0):
        error = 'wrong idx: ' + project['idx']
        return error, False
    return error, True