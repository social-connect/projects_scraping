def check_abstract(project):
    error = ''
    if not ('abstract' in project.keys()):
        error = 'abstract missing'
        return error, False
    if not (isinstance(project['abstract'], str)):
        error = 'abstract is not a string: ' + str(project['abstract'])
        return error, False
    return error, True
