def check_area(project):
    error = ''
    if not ('area' in project.keys()):
        error = 'area missing'
        return error, False
    if not (type(project['area']) is dict):
        error = 'area is not a area: ' + str(project['area'])
        return error, False
    for key in project['area'].keys():
        if (key != 'regions') \
                and (key != 'departements') \
                and (key != 'communes'):
            error = 'unknown area :' + str(project['area'])
    return error, True
