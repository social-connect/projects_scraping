import json
import re
from os import listdir, renames, path
from check_title import check_title
from check_idx import check_idx
from check_spider_name import check_spider_name
from check_img import check_img
from check_abstract import check_abstract
from check_link import check_link
from check_date import check_date
from check_contact import check_contact
from check_state import check_state
from check_project_holder import check_project_holder
from check_partner import check_partner
from check_economic import check_economic
from check_key_words import check_key_words
from check_coordinates import check_coordinates
from check_video import check_video
from check_area import check_area

def check_field(checking_function, field_name, project):
    error, result = checking_function(project)
    if not(result):
        print(field_name, result, error)


def check_results(data_dir, result_path):
    # Processing data files
    # Finding .json files
    files_list = listdir(data_dir)
    files_list = [f for f in files_list if re.match(".*\.json$", f)]

    all_projects = []
    # for each .json we loop on projects
    for file_name in files_list:
        print("reading", file_name)
        decode_json = ""
        features = []

        # reading .json
        with open(data_dir + "/" + file_name) as  projects_file:
            decode_json = json.load(projects_file)
            for project in decode_json:
                check_field(check_idx, 'idx', project)
                check_field(check_spider_name, 'spider name', project)
                check_field(check_title, 'title', project)
                check_field(check_img, 'img', project)
                check_field(check_abstract, 'abstract', project)
                check_field(check_link, 'link', project)
                check_field(check_date, 'date', project)
                check_field(check_key_words, 'key words', project)
                check_field(check_contact, 'contact', project)
                check_field(check_state, 'state', project)
                check_field(check_project_holder, 'project_holder', project)
                check_field(check_partner, 'partner', project)
                check_field(check_economic, 'economic', project)
                check_field(check_video, 'video', project)
                check_field(check_coordinates, 'coordinates', project)
                check_field(check_area, 'area', project)


check_results('../data/spider_results', "")
