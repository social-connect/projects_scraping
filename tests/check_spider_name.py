def check_spider_name(project):
    error = ''
    if not ('spider_name' in project.keys()):
        error = 'spider name missing'
        return error, False
    if not (isinstance(project['spider_name'], str)):
        error = 'spider name is not a string: ' + str(project['spider_name'])
        return error, False
    if not (len(project['spider_name']) > 0):
        error = 'empty spider name'
        return error, False
    return error, True