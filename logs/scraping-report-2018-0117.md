% Rapport de l’indexation des sites de projets d’innovation sociale

=====  =================================================================
Date   17 janvier 2018
Objet  Rapport de l’indexation des sites de projets d’innovation sociale
=====  =================================================================

A ce jour, la démo contient **11595 projets**, pour **29 sites indexés**.

La solution actuellement mise en place (Scrapy & Python) permet une grande flexibilité dans la récupération d'information puisque du code Python peut être adjoint à la configuration de Scrapy pour personnaliser la récupération d'information au maximum. 

Cependant les difficultés suivantes sont régulièrement rencontrées :

* Certains sites sont "'cassés" (erreurs 500 par exemple). Rien de possible dans ce cas tant que le site n'est pas remis en ligne.
* Certains sites utilisent activement du JavaScript et nécessitent d'utiliser un outil de simulation de navigation.
* Certains sites n'ont pas de structure sémantique utilisable. En particulier lorsque le site présente simplement du HTML qui a été saisi dans un éditeur riche, il est quasiment impossible d'en extraire des informations sémantiques d'une fiche projet par exemple la date, les mot-clés ou la localisation.
* Nous extrapolons la forme du HTML que nous "crawlons" à partir d'une ou deux fiche projet par site : il est déjà arrivé que les XPath (chemin sémantique jusqu'a la donnée) choisis fonctionnent pour la majorité des projets, mais dysfonctionnent pour certains à cause d'une donnée supplémentaire qui change la forme de la fiche. Difficile à deviner à moins de parcourir toutes les fiches, ce qui n'est pas envisageable.
* Une difficulté supplémentaire à prévoir est l’évolution des sites en question : toute modification de leurs structures HTML risque beaucoup de casser le "spider" associé.

# Site indexés

* [AG2R La Mondiale](http://www.fondation.ag2rlamondiale.fr)
* [Apriles](http://www.apriles.net/index.php)
* [Avise](http://www.avise.org)
* [Bleu Blanc Zebre](http://bleublanczebre.fr)
* [Bretagne Creative](http://www.bretagne-creative.net)
* [Territoires Conseil](http://www.caissedesdepotsdesterritoires.fr/)
* [Fondation Daniel et Nina Carasso](http://www.fondationcarasso.org)
* [Comment financer mon projet responsable](http://commentfinancermonprojetresponsable.fr)
* [Europe en France](http://www.europe-en-france.gouv.fr)
* [Fondation Bettencourt Schueller](https://www.fondationbs.org)
* [Fondation EDF](https://fondation.edf.com)
* [Fondation MACIF](http://www.fondation-macif.org)
* [Fondation MAIF](https://www.fondation-maif.fr)
* [Fondation RTE](https://www.fondation-rte.org)
* [Le Labo de l’ESS](http://www.lelabo-ess.org)
* [My Positive Impact](http://www.mypositiveimpact.org)
* [CGET](http://lab-egalite-citoyennete.cget.gouv.fr/)
* [Réseau collectivités 53](http://www.reseau-collectivites-53.fr/)
* [Réseau rural](http://www.reseaurural.fr)
* [RTES](http://rtes.fr)
* [Semeoz](http://semeoz.info/)
* [Socialement responsable](https://www.socialement-responsable.org)
* [Solidarum](http://www.solidarum.org)
* [Sparknews](http://www.sparknews.com/)
* [UIA Initiative](http://www.uia-initiative.eu)
* [Urbact](http://urbact.eu)
* [Fondation Veolia](https://www.fondation.veolia.com)
* [ATLAAS](http://www.villes-internet.net)
* [Fondation Vinci](https://www.fondation.veolia.com)

Contributeurs à venir : 6

* [MOT](http://www.espaces-transfrontaliers.org)
* [UNCCAS](http://www.unccas.org)
* [Nos territoires ont de l'avenir](http://www.nosterritoiresontdelavenir.org/)
* [Bruded](https://www.bruded.org/)
* [Fablabo](http://fablabo.net)
* [Experimentations jeunes](http://www.experimentation.jeunes.gouv.fr/)

# Fiches incomplètes


## Sans lien associés

* AG2R La Mondiale
* Fondation Daniel et Nina Carasso
* ATLAAS

## Sans mots clés associés

* Territoires Conseil
* Fondation Bettencourt Schueller
* Fondation EDF
* Fondation MACIF
* Fondation MAIF
* Fondation RTE
* Réseau collectivités 53
* UIA Initiative
* ATLAAS

## Sans contacts

* Bretagne Creative
* Fondation Daniel et Nina Carasso
* Fondation Bettencourt Schueller
* Fondation MACIF
* Fondation MAIF
* Fondation RTE
* Réseau collectivités 53
* Semeoz
* Solidarum
* Sparknews
* Urbact
* Fondation Veolia

## Sans contact

* Bleu Blanc Zebre
* Territoires Conseil
* Fondation Daniel et Nina Carasso
* Comment financer mon projet responsable
* Fondation Bettencourt Schueller
* Fondation MACIF
* Fondation MAIF
* Fondation RTE
* Le Labo de l’ESS
* My Positive Impact
* RTES
* Semeoz
* Sparknews
* UIA Initiative
* Urbact
* ATLAAS

## Sans date

* AG2R La Mondiale
* Apriles
* Comment financer mon projet responsable
* Fondation EDF
* Fondation MACIF
* My Positive Impact
* CGET
* Réseau rural
* Socialement responsable
* Sparknews
* UIA Initiative

## Sans vidéo

* AG2R La Mondiale
* Apriles
* Avise
* Bleu Blanc Zebre
* Bretagne Creative
* Territoires Conseil
* Fondation Daniel et Nina Carasso
* Comment financer mon projet responsable
* Fondation Bettencourt Schueller
* Fondation EDF
* Fondation MACIF
* Fondation MAIF
* Fondation RTE
* Le Labo de l’ESS
* My Positive Impact
* CGET
* Réseau collectivités 53
* Réseau rural
* RTES
* Socialement responsable
* Solidarum
* UIA Initiative
* Urbact
* Fondation Veolia
* ATLAAS
* Fondation Vinci

# Spiders en détail

## AG2R La Mondiale

Il n'y a pas de pagination, les projets sont tous sur la même page de recherche.
Il n'y a pas de lien par projet, le contact est le lien vers le site du projet lui-même.

## Apriles

## Avise

## Bretagne Creative

Possible de récupérer :

* contact
* state = maturité


## Bruded

Nécessite scrapy-splash : pour lancer ce spider, une instance de splash est nécessaire.
Peu de données disponibles.


## Territoires Conseil

Le site dispose d'une API pour récupérer les données, 
le spider l'utilise pour certains paramètres.
Pour les autres, voir `parse_detailed_page`.

Le contact n'est par récupéré complètement.


## Fondation Daniel et Nina Carasso

Pas de fiche par projet. Des critères sont disponibles pour filter sur la région, mais ne sont pas présents sur la page.

## Europe en France

## Experimentations jeunes spider

## Fablabo

N'est peut-être pas dans la liste des contributeurs officiels ?

## Fondation Bettencourt Schueller

Pas de mot-clés présents.

## Fondation EDF

## Fondation MACIF

La pagination fonctionne en ajax et nécessite d'utiliser Splash


## Fondation MAIF

La pagination fonctionne en ajax et nécessite d'utiliser Splash


## Fondation RTE

Les mot-clés sont identifiés par un picto. Le script récupère le nom du porteur de projet, mais la fiche contient également une information sur la structure.


## CGET

Certaines fiches contiennent également la communes, mais elle n'est pas récupérée.


## Le Labo de l’ESS

Utilise scrapy-splash: nécessite une instance de splash

La région est présente sur les filtres mais pas dans les fiches, donc non récupérable.


## MOT

Le contenu est dans un champs texte riche non structuré, il est difficile de récupérér les données, par exemple les mot-clés 


## My Positive Impact

## Nos territoires ont de l'avenir

Nécessite Splash, les données sont difficiles à indexer


## Réseau collectivités 53

Des filtres par domaines existent mais l'information n'est pas présente sur la fiche.


## Réseau rural

Nécessite Splash pour indexer les données.


## RTES

La localisation est mélée aux mot-clés.


## Semeoz

La date est au format français.
On pourrait récupérer le texte complet plutôt que le résumé.


## Socialement responsable

Voir si le type de structure peut renseigner le modèle économique.


## Solidarum

La date est au format "jj.mm.aaaa".


## Sparknews

La vidéo n'est pas récupérée alors qu'elle existe.


## UIA Initiative

## Urbact

Pas de page suivante.
Deux xpath possibles pour les mot-clés : themes ou topics.


## UNCCAS

## Fondation Veolia

## ATLAAS

Ce spider utilise l'API disponible "/getAtlaasActions?z=n&p="


## Fondation Vinci

Une seule page de résultats, pas besoin de "next_page"

