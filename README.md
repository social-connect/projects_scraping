# CGET Scraping

This tool get data from many sources to be used by meta search engine CGET.

## Install

Install libraries

### Using `pip`

    pip install -r requirements.txt

### Using `pipenv`

If you have [pipenv](https://docs.pipenv.org/) installed in your system a Pipfile is available.

    pipenv install

*See more information to [install pipenv](https://docs.pipenv.org/install.html#installing-pipenv)*
    
## Using

### To crawl manually an existing spider

Run

    scrapy crawl <spider_name>

where `<spider_name>` is `name` attribute in a spider script in `cget_scraping/spiders/apriles_spider.py`.

A JsonExporter pipeline send data in json files in `data` folder.

### Using python scripts

### To crawl all spiders in a single Json file 

```buildoutcfg
python launch_spiders.py
```
* Results for each spider are stored in the directory **cget_scraping/data/spider_results**
* Results of all spiders are store in **data/allprojects.json**

### To crawl some selected spiders in separated Json file

```
python launch_selected_spiders.py -s SpiderClassName1 SpiderClassName2 ...
```

Results for each spider are stored in the the directory **cget_scraping/data/spider_results**

## Add a spider

### Spider fields

The following fields must be populated by the spider:

* **title** [required]: the project name 
* **img**:  url of a single image to illustrate the project or an empty string
* **abstract**:  textual description of the project or an empty string
* **link** [required]: url to the scrawled page 
* **date** : date of the project or null
* **area**: an empty dictionnary or a dictionnary with one or several fields between the following :
    * **region**
    * **communes**
    * **departments**
    * **not_filtered**
* **key_words**: a list of key words or an empty list
* **contact**: contact to know more about the project or an empty string
* **video**: url of a single video or an empty string
* **state**: project state or an empty list
* **project_holder**: project holder or an empty list
* **partner**: list of partners of the project or an empty list
* **economic**: TODO

In the pipeline, the following fields will be automatically added:

* **idx** [required]: index build from spider name and incremental integer
* **spider_name** [required]: the name of the spider
* **coordinates**: location [longitude, latitude] of the project or an empty list 

### Add spider to the project

1. Generate the spider python file in **cget_scraping/cget_scraping/spiders** directory (see below)
2. Import the spider file in **__init__.py** file of spiders directory
3. Update the variable **ALL_SPIDERS** in **__init__.py** file of spiders directory

### Generate a spider from templates

Two templates are available in the project in `cget_scraping/templates` (run `scrapy genspider -l` to list them):

* `cget` for simple website
* `cgetsplash` for website using javascript to display data

To create a new spider run for example :

```
scrapy genspider -t cget mysite mysite.org
```

### Test spider 

```
cd cget_scraping/tests
python check_spider_results.py
```

For each spider results json file stored in **cget_scraping/data/spider_results** : 

* check that spiders provide all fields
* check that required fields are not empty
* check that the type of each field is right

If only the name of the file is printed, tests are ok. If an error is detected, the description of this error is printed.

### Crawl data rendered by javascript

Use plugin [Scrapy-splash](https://github.com/scrapy-plugins/scrapy-splash) with [Splash scriptable browser](https://github.com/scrapinghub/splash) to render javascript.

## Playing with json using jq bash command

See [Manipulate json files with `jw`](docs/en/manipulate-json.md)
