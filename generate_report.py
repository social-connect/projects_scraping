# -*- coding: utf-8 -*-
from datetime import datetime
from utils.spider_infos import get_spiders_missing_xpath
from cget_scraping.spiders import DEMO_SPIDERS, ALL_SPIDERS

def generate_content_report():
    """Generate content report
    return string
    """

    content = ""
    content += "% Rapport de l’indexation des sites de projets d’innovation sociale"
    content += """

:Date: {:%d %B %Y}
:Objet: Rapport de l’indexation des sites de projets d’innovation sociale

""".format(datetime.now())

    content += "A ce jour, la démo contient **11653 projets**, pour **{} sites indexés**.".format(len(DEMO_SPIDERS))

    content += """
La solution actuellement mise en place (Scrapy & Python) permet une grande flexibilité dans la récupération d'information puisque du code Python peut être adjoint à la configuration de Scrapy pour personnaliser la récupération d'information au maximum. 

Cependant les difficultés suivantes sont régulièrement rencontrées :

* Certains sites sont "'cassés" (erreurs 500 par exemple). Rien de possible dans ce cas tant que le site n'est pas remis en ligne.
* Certains sites utilisent activement du JavaScript et nécessitent d'utiliser un outil de simulation de navigation.
* Certains sites n'ont pas de structure sémantique utilisable. En particulier lorsque le site présente simplement du HTML qui a été saisi dans un éditeur riche, il est quasiment impossible d'en extraire des informations sémantiques d'une fiche projet par exemple la date, les mot-clés ou la localisation.
* Nous extrapolons la forme du HTML que nous "crawlons" à partir d'une ou deux fiche projet par site : il est déjà arrivé que les XPath (chemin sémantique jusqu'a la donnée) choisis fonctionnent pour la majorité des projets, mais dysfonctionnent pour certains à cause d'une donnée supplémentaire qui change la forme de la fiche. Difficile à deviner à moins de parcourir toutes les fiches, ce qui n'est pas envisageable.
* Une difficulté supplémentaire à prévoir est l’évolution des sites en question : toute modification de leurs structures HTML risque beaucoup de casser le "spider" associé.

# Site indexés
"""

    for spider in DEMO_SPIDERS:
        name = spider.name
        if hasattr(spider, 'label'):
            name = spider.label
        content += '\n* [{}]({})'.format(name, spider.page_url)

    # Spider to be included
    missing_spiders = set(ALL_SPIDERS).difference(DEMO_SPIDERS)
    content += "\nContributeurs à venir : {}\n".format(len(missing_spiders))
    for spider in missing_spiders:
        name = spider.name
        if hasattr(spider, 'label'):
            name = spider.label
        content += '\n* [{}]({})'.format(name, spider.page_url)

    content += "\n\n# Fiches incomplètes\n"

    spiders_missing_xpath = get_spiders_missing_xpath()

    content += "\n\n## Sans lien associés\n"
    for spider in spiders_missing_xpath['link']:
        content += u"\n* {}".format(spider.label or spider.name)

    content += "\n\n## Sans mots clés associés\n"
    for spider in spiders_missing_xpath['keywords']:
        content += "\n* {}".format(spider.label or spider.name)

    content += "\n\n## Sans contacts\n"
    for spider in spiders_missing_xpath['contact']:
        content += "\n* {}".format(spider.label or spider.name)

    content += "\n\n## Sans contact\n"
    for spider in spiders_missing_xpath['area']:
        content += "\n* {}".format(spider.label or spider.name)

    content += "\n\n## Sans date\n"
    for spider in spiders_missing_xpath['date']:
        content += "\n* {}".format(spider.label or spider.name)

    content += "\n\n## Sans vidéo\n"
    for spider in spiders_missing_xpath['video']:
        content += "\n* {}".format(spider.label or spider.name)

    content += "\n\n# Spiders en détail"

    for spider in ALL_SPIDERS:
        content += "\n\n## {}".format(spider.__doc__)

    return content


def main():
    """Main programm"""
    content = generate_content_report()
    report_filename = 'logs/scraping-report-{:%Y-%m-%d}.md'.format(datetime.now())
    report_file = open(report_filename, 'w')
    report_file.write(content)


if __name__ == '__main__':
    main()
