# -*- coding: utf-8 -*-
import requests
import json
import logging
from slugify import slugify
from urllib.parse import quote

from scrapy import signals
from scrapy.exceptions import DropItem
from scrapy.exporters import JsonItemExporter

from cget_scraping.geo_loc.geo_coding_getter import GeoCodingGetter


# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class JsonExportPipeline(object):
    """
    Json writer pipeline
    """
    idx = 0
    geo_loc_getter = GeoCodingGetter()
    def __init__(self):
        self.files = {}

    def check_fields(self, item):
        item['img'] = item['img'] or ''
        item['abstract'] = item['abstract'] or ''
        item['date'] = item['date'] or ''
        if not (type(item['key_words']) is list):
            item['key_words'] = [item['key_words'] or '']
        item['key_words'] = item['key_words'] or []
        item['contact'] = item['contact'] or ''
        if not (type(item['video']) is list):
            item['video'] = [item['video'] or '']
        item['video'] = item['video'] or []
        item['state'] = item['state'] or ''
        item['project_holder'] = item['project_holder'] or ''
        if not (type(item['partner']) is list):
            item['partner'] = [item['partner'] or '']
        item['partner'] = item['partner'] or []
        item['economic'] = item['economic'] or ''

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self, spider):
        file = open('data/spider_results/%s_projects.json' % spider.name, 'w+b')
        self.files[spider] = file
        self.exporter = JsonItemExporter(file)
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()

    def process_item(self, item, spider):
        item['idx'] = JsonExportPipeline.idx
        JsonExportPipeline.idx += 1
        item['spider_name'] = spider.name
        res_geoloc = self.geo_loc_getter.get_geolocalisation(spider.name, item['title'], item['area'])
        item['coordinates'] = res_geoloc['coordinates']
        self.check_fields(item)
        self.exporter.export_item(item)
        return item


class RestExportPipeline(object):
    """
    REST API writer pipeline
    """
    # base_url = 'http://localhost:8000/'
    base_url = 'https://social-connect-cget.herokuapp.com/'
    token_file = open('token.env')
    token = token_file.read().strip()
    contributor_id = ""
    geo_loc_getter = GeoCodingGetter()
    logger = logging.getLogger(__name__)

    def __init__(self):
        self.files = {}
        self.headers = {
            'Content-Type':'application/json',
            'Accept': 'application/json',
            'X-CSRFToken': 'KuYw8JixZqPNIa2F4sah5r65Es9PSMnplUua7VPuMkmYMRhNJ0UhGkYAOqJBugk4',
            'Authorization': 'Token ' + self.token
        }

    @classmethod
    def from_crawler(cls, crawler):
        """From crawler"""
        pipeline = cls()
        return pipeline

    def open_spider(self, spider):
        """
        Get contributor at open spider
        """
        self.contributor_id = self.process_contributor(spider)

    def process_keywords(self, keywords):
        """
        Add keywords in database using REST API
        :param keywords: scrapperd keywords
        :return keywords indices in database
        """
        keywords_url = '{}api/keywords/'.format(self.base_url)

        # format keywords
        if not isinstance(keywords, list):
            keywords = [keywords or '']

        # Store keywords indices in database
        keywords_id = []
        for keyword in keywords:
            # check if keyword exists in database
            slugified_keyword = slugify(keyword.replace("'", ""))
            existing_keyword = requests.get(
                '{}?value={}'.format(keywords_url, slugified_keyword)
            ).json()['results']

            if len(existing_keyword) > 0:
                keywords_id.append(existing_keyword[0]['id'])
            else:
                keyword = keyword[0].upper() + keyword[1:]
                if len(keyword) > 5000:
                    # truncate keyword to match with Django model
                    keyword = keyword[0:5000]
                # post keyword to database using REST API
                data = {'label':keyword}
                response = requests.post(keywords_url, data=json.dumps(data), headers=self.headers)
                keywords_id.append(response.json()['id'])

        return keywords_id

    def process_contributor(self, spider):
        """
        Add contributor to database using REST API
        :param spider: spider information
        :return: contributor id in database
        """
        contributor_url = self.base_url + 'api/contributors/'

        # check if contributor is in database
        existing_contributor = requests.get(
            '{}?spider_name={}'.format(contributor_url, spider.name)
        ).json()['results']

        if len(existing_contributor) > 0:
            return existing_contributor[0]['id']

        data = {
            'name': spider.label,
            'spider_name': spider.name,
            'link': spider.page_url
        }
        # truncate to match with Django model
        if len(data['name']) > 1000:
            data['name'] = data['name'][0:1000]
        if len(data['spider_name']) > 1000:
            data['spider_name'] = data['spider_name'][0:1000]

        # post contributor in database
        response = requests.post(contributor_url, data=json.dumps(data), headers=self.headers)
        return response.json()['id']

    def check_fields(self, item):
        """
        Format scrapped data according Django model
        :param item: scrapped data
        """
        if len(item['title']) > 5000:
            #truncate title
            item['title'] = item['title'][0:5000]
        item['img'] = item['img'] or ''
        item['abstract'] = item['abstract'] or ''
        item['date'] = item['date'] or ''
        if len(item['date']) > 5000:
            #truncate date
            item['date'] = item['date'][0:5000]
        if not isinstance(item['key_words'], list):
            item['key_words'] = [item['key_words'] or '']
        item['key_words'] = item['key_words'] or []
        item['contact'] = item['contact'] or ''
        if len(item['contact']) > 5000:
            #truncate contact
            item['contact'] = item['contact'][0:5000]
        if not isinstance(item['video'], list):
            item['video'] = [item['video'] or '']
        item['video'] = item['video'] or []
        item['state'] = item['state'] or ''
        if len(item['state']) > 1000:
            #truncate state
            item['state'] = item['state'][0:1000]
        item['project_holder'] = item['project_holder'] or ''
        if len(item['project_holder']) > 5000:
            #truncate project_holder
            item['project_holder'] = item['project_holder'][0:5000]
        if not isinstance((item['partner']), list):
            item['partner'] = [item['partner'] or '']
        item['partner'] = item['partner'] or []
        item['economic'] = item['economic'] or ''
        if len(item['economic']) > 5000:
            #truncate economic
            item['economic'] = item['economic'][0:5000]

    def process_project(self, item, spider, keywords_id, contributor_id):
        """
        Add project in database
        :param item: scrapped data
        :param spider: spider information
        :param keywords_id: keywords indices in database
        :param contributor_id: contrutor index in database
        :return: project_id in database
        """
        # get location
        res_geoloc = self.geo_loc_getter.get_geolocalisation(spider.name, item['title'], item['area'])
        # store coordinates
        item['coordinates'] = res_geoloc['coordinates']
        # check the data match with Django model constrainst
        self.check_fields(item)
        projects_url = '{}api/projects/'.format(self.base_url)

        # test if project is in database
        existing_project = requests.get(
            '{}?title={}&contributor={}'.format(projects_url, item['title'], contributor_id)
        ).json()['results']

        if len(existing_project) > 0:
            return existing_project[0]['id']

        data = {
            'title': item['title'],
            'abstract': item['abstract'],
            'link': quote(item['link'], safe='/:'),
            'date' : item['date'],
            'keywords_id': keywords_id,
            'area': str(item['area']),
            'contributor_id': contributor_id,
            'contact': item['contact'],
            'project_holder': item['project_holder'],
            'img' : quote(item['img'], safe='/:'),
            'coordinates': item['coordinates'],
            'state': item['state'],
            'partner' : str(item['partner']),
            'economic' : item['economic'],
            'video': str(item['video']),
        }
        # add coordinates if they are known
        if len(item['coordinates']) == 2:
            data['longitude'] = item['coordinates'][0]
            data['latitude'] = item['coordinates'][1]
        # add project in database

        response = requests.post(projects_url, data=json.dumps(data), headers=self.headers)
        if response.status_code == 201:
            return response.json()['id']
        return None

    def process_item(self, item, spider):
        # add keywords in database
        keywords_id = []
        keywords_id = self.process_keywords(item['key_words'])
        # add project in database
        project_id = self.process_project(item, spider, keywords_id, self.contributor_id)
        return item
