import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class FondationRTESpider(scrapy.Spider, CgetSpider):
    """Fondation RTE

Les mot-clés sont identifiés par un picto. Le script récupère le nom du porteur de projet, mais la fiche contient également une information sur la structure.
"""
    LIMIT = 36
    name = "fondationrte"
    label = "Fondation RTE"
    page_url = "https://www.fondation-rte.org"
    start_urls = ['https://www.fondation-rte.org/actions/rechercher-un-projet/', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//div[@id="content-project-list"]/article'
    next_page_xpath = '//div[@id="project-list"]/div[@class="carte-pagination"]//a[@class="next page-numbers"]/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './header/img[@class!="picto"]/@src'
    link_xpath = './header/h2/a/@href'
    title_xpath = './header/h2/a/text()'
    economic_xpath = './header/img[@class="picto"]/@title'

    # In action page
    date_xpath = '//div[@id="content"]/article/header/div[@class="s-title"]/span[@class="date"]/text()'
    abstract_xpath = '//div[@id="project-content-left"]/div'
    project_holder_xpath = '//div[@span="sponsor-name"]/text()'

    # Not available
    key_words_xpath = ''
    contact_xpath = ''
    area_xpath = ''
    video_xpath = ''
    state_xpath = ''
    partner_xpath = ''

    def parse(self, response):
        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']

            # If a URL was found, go check it out.
            if url:
                yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(FondationRTESpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


