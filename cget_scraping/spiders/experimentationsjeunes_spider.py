import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class ExperimentationsJeunesSpider(scrapy.Spider, CgetSpider):
    """Experimentations jeunes spider"""
    LIMIT = 100
    name = "experimentationsjeunes"
    label = "Experimentations jeunes"
    page_url = "http://www.experimentation.jeunes.gouv.fr/"
    start_urls = ['http://www.experimentation.jeunes.gouv.fr/spip.php?appel-a-projet=&exp_porteur_projet=&exp_thematique=&exp_etat=&exp_type_structure=&page=recherche-exp', ]

    action_list_xpath = '//div[@id="content"]//div[@class="article"]'
    next_page_xpath = '//ul[@class="pager"]/li[last()]/a/@href'

    # All the xpaths needed to fill the Action objects attributes
    link_xpath = './a[@class="lirelasuite"]/@href'

    # In action page
    title_xpath = '//div[@id="content"]//h2[@class="title2"]/text()'
    abstract_xpath = '//div[@id="content"]//div[@class="cnt"]//h3[contains(./text(), "Résumé")]//following-sibling::p/text()'
    date_xpath = '//div[@id="content"]//div[@class="cnt"]//h3[contains(./text(), "Calendrier")]//following-sibling::p/text()'
    img_xpath = ''
    area_xpath = '//table[@class="tableau_etat"]//td[contains(./text(), "Région")]/b/text()'
    key_words_xpath = '//table[@class="tableau_etat"]//td[contains(./text(), "Thématique")]/b/text()'
    contact_xpath = ''
    video_xpath = ''
    state_xpath = '//div[starts-with(./@class, "image")]/@class'
    project_holder_xpath = '//table[@class="tableau_etat"]//td[contains(./text(), "Porteur du projet")]/following-sibling::td/b/text()'
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        response.meta['item']['title'] = self.get_title(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_area(self, action):
        regions = self.extract_list(action, self.area_xpath, data_type="area")
        return { 'regions': regions }

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(ExperimentationsJeunesSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


