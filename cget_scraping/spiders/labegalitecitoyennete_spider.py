import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class LabEgaliteCitoyenneteSpider(scrapy.Spider, CgetSpider):
    """CGET

Certaines fiches contiennent également la communes, mais elle n'est pas récupérée.
"""
    LIMIT = 5
    name = "labegalitecitoyennete"
    label = "CGET"
    page_url = "http://lab-egalite-citoyennete.cget.gouv.fr/"
    start_urls = ['http://lab-egalite-citoyennete.cget.gouv.fr/les-projets', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//div[contains(@class, "view-les-projets")]/div[@class="view-content"]/div'
    next_page_xpath = '//li[@class="next"]/a/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = '//div[contains(@class, "photo-image")]/div[contains(@class, "field-name-field-logo-du-projet")]/div/div/img[contains(@class, "img-responsive")]/@src'
    link_xpath = './/a/@href'
    abstract_xpath = '//div[contains(@class, "field-name-field-descriptif-libre-de-l-acti")]/div[@class="field-items"]/div[@class="field-item even"]/node()'
    title_xpath = './/a/text()'
    date_xpath = ''

    # In action page
    area_xpath = '//div[contains(@class, "field-name-field-region")]/div[@class="field-items"]/div/text()'
    key_words_xpath = '//div[contains(@class, "field-name-field-domaines-daction")]/div[@class="field-items"]/div/text()'
    contact_xpath = '(//div[@class="groupe"])[1]/a/@href'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = 'http://lab-egalite-citoyennete.cget.gouv.fr/' + self.get_contact(response)[3:]
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_area(self, action):
        regions = self.extract_list_by_xpath(action, self.area_xpath, data_type="area")
        if len(regions) == 0:
            return {}
        return {'regions': regions }

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(LabEgaliteCitoyenneteSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


