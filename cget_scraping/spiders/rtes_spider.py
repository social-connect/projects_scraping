"""
NOT finnished
"""
import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class RtesSpider(scrapy.Spider, CgetSpider):
    """RTES

La localisation est mélée aux mot-clés.
"""
    LIMIT = 48
    name = "rtes"
    label = "RTES"
    page_url = "http://rtes.fr"
    start_urls = [
        'http://rtes.fr/Formation-RTES',
        'http://rtes.fr/Les-PTCE',
        'http://rtes.fr/Journees-d-echanges-et-de-debats',
        'http://rtes.fr/Renforcer-l-ESS-dans-les',
    ]

    parsed_urls = []

    action_list_xpath = '//div[@id="last"]/ul/li'
    next_page_xpath = '//div[@class="pagination"]//strong[@class="on"]/following-sibling::a[@class="lien_pagination"]/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = './h2//a/@href'
    abstract_xpath = './/a[img]/following-sibling::div/text()'
    title_xpath = './h2//a/text()'
    date_xpath = '//span[@class="date"]/text()'

    # In action page
    area_xpath = ''
    key_words_xpath = '//div[@id="mots_article"]/ul/li/a/text()'
    contact_xpath = '//span[@class="vcard author"]/a/@href'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):
        self.parsed_urls.append(response.url)

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # *response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # *response.meta['item']['img'] = self.get_img(response)
        # *response.meta['item']['title'] = self.get_title(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(RtesSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def spider_closed(self, spider):

        # Some status
        self.logger.info("**** Spider closed: {} ****".format(spider.name))
        self.logger.info("--- {} Items retrieved (main search page count)".format(self.item_count))
        self.logger.info("--- {} Items retrieved (detailed page count)".format(self.item_count_depth_1))
        self.logger.info("--- {} Pages crawled".format(self.page_count))
        self.logger.info("--- {} Pages crawled (parsed_urls list count)".format(len(self.parsed_urls)))
        # self.logger.info("---parsed urls :")
        # for url in self.parsed_urls:
        #     self.logger.info("{}".format(url))
        self.print_error()
        self.logger.info("***End scraping***\n")

    def get_contact(self, action):
        return self.get_link(action, self.contact_xpath, "contact")

    def get_key_words(self, action):
        return self.extract_list_by_xpath(action, self.key_words_xpath, "key_words")
