import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class BretagneCreativeSpider(scrapy.Spider, CgetSpider):
    """Bretagne Creative

Possible de récupérer :

* contact
* state = maturité
"""
    LIMIT = 261
    name = "bretagne_creative"
    label = "Bretagne Creative"
    page_url = "http://www.bretagne-creative.net"
    start_urls = ['http://www.bretagne-creative.net/'
                  'rubrique2.html?debut_articles=0&theme=', ]

    list_css_selector = 'ul > li div.vignette'
    next_page_xpath = '//span[@class="next"]/a/@href'

    img_xpath = './/img/@src'
    link_xpath = './/a/@href'
    abstract_xpath = './/div/text()'
    title_xpath = './/h3//text()'

    # In action page
    date_xpath = '//div[@id="extra1"]//tr/td[1]'
    area_xpath = '//div[@id="extra1"]//tr/td[1]'
    key_words_xpath = '//div[@id="extra1"]//tr/td[1]'
    contact_xpath = ""
    video_xpath = ""
    state_xpath = ""
    project_holder_xpath = '//div[@id="extra1"]//tr/td[1]'
    partner_xpath = ""
    economic_xpath = ""

    def parse(self, response):
        for action in response.css(self.list_css_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(BretagneCreativeSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def parse_detailed_page(self, response):
        self.item_count_depth_1 += 1
        response.meta['item']['date'] = self.get_date(response)
        response.meta['item']['project_holder'] = self.get_project_holder(response)
        response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        yield response.meta['item']

    def get_date(self, action):
        try:
            labels = action.xpath(self.date_xpath)
            for label in labels:
                label_text = label.xpath('./text()').extract_first()
                if label_text == "Mise à jour":
                    date = label.xpath('./following-sibling::td/text()').extract_first().strip()
                    return date
            return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "abstract item n <{}> " \
                    "page <{}>".format(e, self.item_count_depth_1, self.page_count)
            self.error_array.append(error)
            return None

    def get_project_holder(self, action):
        try:
            labels = action.xpath(self.project_holder_xpath)
            for label in labels:
                label_text = label.xpath('./text()').extract_first()
                if label_text == "Auteurs":
                    data = " ".join(label.xpath('./following-sibling::td//a/text()').extract())
                    return data
            return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "abstract item n <{}> " \
                    "page <{}>".format(e, self.item_count_depth_1, self.page_count)
            self.error_array.append(error)
            return None

    def get_area(self, action):
        try:
            labels = action.xpath(self.area_xpath)
            for label in labels:
                label_text = label.xpath('./text()').extract_first()
                if label_text == "Pays":
                    data = label.xpath('./following-sibling::td/a/text()').extract()
                    return {'collectivites_locales' : data }
            return {}
        except AttributeError as e:
            error = "Error <{}> on " \
                    "abstract item n <{}> " \
                    "page <{}>".format(e, self.item_count_depth_1, self.page_count)
            self.error_array.append(error)
            return {}

    def get_key_words(self, action):
        try:
            labels = action.xpath(self.key_words_xpath)
            for label in labels:
                label_text = label.xpath('./text()').extract_first()
                if label_text == "Thèmes":
                    data = label.xpath('./following-sibling::td/a/text()').extract()
                    return data
            return []
        except AttributeError as e:
            error = "Error <{}> on " \
                    "abstract item n <{}> " \
                    "page <{}>".format(e, self.item_count_depth_1, self.page_count)
            self.error_array.append(error)
            return None

    def get_title(self, action):
        try:
            return "".join(action.xpath(self.title_xpath).extract()).strip()
        except AttributeError as e:
            self.error_array.append(e)
            return None