import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class SolidarumSpider(scrapy.Spider, CgetSpider):
    """Solidarum

La date est au format "jj.mm.aaaa".
"""
    LIMIT = 21
    name = "solidarum"
    label = "Solidarum"
    page_url = "http://www.solidarum.org"
    start_urls = ['http://www.solidarum.org/recherche', ]

    list_css_selector = 'div#main-content ul.clearfix li.item'
    next_page_xpath = '//a[@title="Aller à la page suivante"]/@href'

    #In search page
    img_xpath = './/img/@src'
    link_xpath = './/h4[@class="title"]/a/@href'
    abstract_xpath = './/div[@class="field-items"]//p/text()'
    title_xpath = './/h4[@class="title"]/a/text()'
    date_xpath = './/div[@class="meta"]/span[@class="post-date"]/text()'

    # In action page
    area_xpath = '//div[@class="s-tags lieu"]/a/text()'
    key_words_xpath = '//div[@class="s-tags domaine"]/a/text()'
    contact_xpath = ''
    video_xpath = ""
    state_xpath = ""
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ""

    def parse(self, response):
        for action in response.css(self.list_css_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(SolidarumSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def parse_detailed_page(self, response):
        self.item_count_depth_1 += 1
        response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        yield response.meta['item']

    def get_area(self, action):
        try:
            areas = action.xpath(self.area_xpath).extract()
            return { 'not_filtered' : areas }
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "area", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_key_words(self, action):
        try:
            key_words = action.xpath(self.key_words_xpath).extract()
            return key_words
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "key_words", self.item_count, self.page_count)
            self.error_array.append(error)
            return None
