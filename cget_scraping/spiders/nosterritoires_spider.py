import scrapy
from scrapy_splash import SplashRequest # If results are provided by javascript
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class NosterritoiresSpider(scrapy.Spider, CgetSpider):
    """Nos territoires ont de l'avenir

Nécessite Splash, les données sont difficiles à indexer
"""
    LIMIT = 20
    name = "nosterritoires"
    label = "Nos territoires ont de l'avenir"
    page_url = "http://www.nosterritoiresontdelavenir.org/"
    start_urls = ['http://www.nosterritoiresontdelavenir.org/', ]

    action_list_xpath = '//a[@role="button"]'
    next_page_xpath = ''

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = '//div[@id="SITE_PAGES"]//img/@src'
    link_xpath = './@href'
    abstract_xpath = ''
    title_xpath = ''
    date_xpath = ''

    # In action page
    area_xpath = ''
    key_words_xpath = ''
    contact_xpath = ''
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def start_requests(self):
        """Request the urls using Splash to interprete the javascript too"""
        for url in self.start_urls:
            yield SplashRequest(url, self.parse, args={'wait': 1.5})

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield SplashRequest(
                url, 
                callback=self.parse_detailed_page, 
                meta={'item': item}, 
                args={'wait': 20.0}
            )

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        # is_next_page, next_page = self.get_next_page(response)
        # if is_next_page:
        #     yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(NosterritoiresSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


