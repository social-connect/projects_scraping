# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

from .ag2rlamondiale_spider import Ag2rlamondialeSpider
from .apriles_spider import AprilesSpider
from .avise_spider import AviseSpider
from .bleublanczebre_spider import BleuBlancZebreSpider
from .bretagne_creative_spider import BretagneCreativeSpider
from .bruded_spider import BrudedSpider
from .caissedepotterritoires_spider import CaisseDepotTerritoiresSpider
from .carasso_spider import CarassoSpider
from .commentfinancermonprojetresponsable_spider import CommentFinancerMonProjetResponsableSpider
from .europeenfrance_spider import EuropeenfranceSpider
from .experimentationsjeunes_spider import ExperimentationsJeunesSpider
from .fablabo_spider import FablaboSpider
from .fondationbs_spider import FondationBSSpider
from .fondationcaritasfrance_spider import FondationCaritasFranceSpider
from .fondationedf_spider import FondationEDFSpider
from .fondationmacif_spider import FondationMacifSpider
from .fondationmaif_spider import FondationMaifSpider
from .fondationrte_spider import FondationRTESpider
from .labegalitecitoyennete_spider import LabEgaliteCitoyenneteSpider
from .lelaboess_spider import LelaboessSpider
from .mot_spider import MotSpider
from .mypositiveimpact_spider import MyPositiveImpactSpider
from .nosterritoires_spider import NosterritoiresSpider
from .reseaucollectivite53_spider import ReseauCollectivite53
from .reseaurural_spider import ReseaururalSpider
from .rtes_spider import RtesSpider
from .semeoz_spider import SemeozSpider
from .socialementresponsable_spider import SocialementresponsableSpider
from .solidarum_spider import SolidarumSpider
from .sparknews_spider import SparkNewsSpider
from .uiainitiative_spider import UIAInitiativeSpider
from .unccas_spider import UnccasSpider
from .urbact_spider import UrbactSpider
from .veolia_spider import VeoliaSpider
from .villesinternet_spider import VillesInternetSpider
from .vinci_spider import VinciSpider

NOK_SPIDERS = [
    FondationCaritasFranceSpider,
]

USING_SPLASH_SPIDERS = [
    BrudedSpider, NosterritoiresSpider, UnccasSpider, \
    ReseaururalSpider, FondationMacifSpider, FondationMaifSpider
]

ALL_SPIDERS = [
    Ag2rlamondialeSpider, AprilesSpider, AviseSpider, \
    BretagneCreativeSpider, BrudedSpider, \
    CaisseDepotTerritoiresSpider, CarassoSpider, \
    EuropeenfranceSpider, ExperimentationsJeunesSpider, \
    FablaboSpider, FondationCaritasFranceSpider, \
    FondationBSSpider, FondationEDFSpider, \
    FondationMacifSpider, FondationMaifSpider, FondationRTESpider,\
    LabEgaliteCitoyenneteSpider, \
    LelaboessSpider, MotSpider, MyPositiveImpactSpider, NosterritoiresSpider, \
    ReseauCollectivite53, ReseaururalSpider, RtesSpider, SemeozSpider,\
    SocialementresponsableSpider, SolidarumSpider, \
    SparkNewsSpider, UIAInitiativeSpider, UrbactSpider, UnccasSpider,\
    VeoliaSpider, VillesInternetSpider, VinciSpider
]

DEMO_SPIDERS = [
    Ag2rlamondialeSpider, AprilesSpider, AviseSpider, \
    BleuBlancZebreSpider, BretagneCreativeSpider, \
    CaisseDepotTerritoiresSpider, CarassoSpider, \
    CommentFinancerMonProjetResponsableSpider, \
    EuropeenfranceSpider, ExperimentationsJeunesSpider, \
    FondationBSSpider, FondationCaritasFranceSpider, \
    FondationEDFSpider, FondationMacifSpider, \
    FondationMaifSpider, FondationRTESpider, \
    LabEgaliteCitoyenneteSpider, \
    LelaboessSpider, MotSpider, MyPositiveImpactSpider, \
    ReseauCollectivite53, RtesSpider, \
    SemeozSpider, SocialementresponsableSpider, SolidarumSpider, \
    SparkNewsSpider, UIAInitiativeSpider, UrbactSpider, \
    VeoliaSpider, VillesInternetSpider, VinciSpider
]
