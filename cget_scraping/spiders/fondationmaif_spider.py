import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class FondationMaifSpider(scrapy.Spider, CgetSpider):
    """Fondation MAIF

La pagination fonctionne en ajax et nécessite d'utiliser Splash
"""
    LIMIT = 1
    name = "fondationmaif"
    label = "Fondation MAIF"
    page_url = "https://www.fondation-maif.fr"
    start_urls = ['https://www.fondation-maif.fr/rubriques.php?rub=1&archive=0', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//ul[@class="row mdc-masonry-grid"]/li/article'
    next_page_xpath = '//div[@id="block-system-main"]/div[@class="item-list"]/ul/li[@class="pager-next"]/a/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './div[@class="entry-thumb"]/img/@src'
    link_xpath = './div[@class="entry-content"]/h3/a/@href'
    title_xpath = './div[@class="entry-content"]/h3/a/text()'
    abstract_xpath = './div[@class="entry-content"]/span[@class="sub-title"]/text()'

    # In action page
    date_xpath = '//div[@class="jumbotron blocIdentite"]//span[contains(text(), "Date")]/../../div/p/descendant-or-self::*/text()'
    partner_xpath = '//div[@class="jumbotron blocIdentite"]//span[contains(text(), "partenaire")]/../../div/p/descendant-or-self::*/text()'

    # Not available
    economic_xpath = ''
    key_words_xpath = ''
    project_holder_xpath = ''
    contact_xpath = ''
    area_xpath = ''
    video_xpath = ''
    state_xpath = ''

    def parse(self, response):
        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']

            # If a URL was found, go check it out.
            if url:
                yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        # is_next_page, next_page = self.get_next_page(response)
        # if is_next_page:
        #     yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_partner(self, action):
        return self.extract_string_data_from_list(action, self.partner_xpath, 'partner', ' ')

    def get_date(self, action):
        return self.extract_string_data_from_list(action, self.date_xpath, 'date', ' ')

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(FondationMaifSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider
