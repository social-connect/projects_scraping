import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class SocialementresponsableSpider(scrapy.Spider, CgetSpider):
    """Socialement responsable

Voir si le type de structure peut renseigner le modèle économique.
"""
    LIMIT = 187
    download_delay = 2  # need a delay or the site blocks requests
    name = "socialementresponsable"
    label = "Socialement responsable"
    page_url = "https://www.socialement-responsable.org"
    start_urls = ['https://www.socialement-responsable.org/annuaire', ]

    action_list_xpath = '//div[@class="content-wrapper"]//article'
    next_page_xpath = '//a[@rel="next"]/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = './/h2//a/@href'
    abstract_xpath = '//div[@class="structure-type"]/text()'
    title_xpath = './/h2//text()'
    date_xpath = ''

    # In action page
    area_xpath = './/p[@class="address"]/text()'
    key_words_xpath = '//div[contains(text(), "Activité")]/following-sibling::div//text()'
    contact_xpath = '//div[contains(text(), "Contact(s)")]/following-sibling::div//a/text()'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = './/h2//text()'
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # *response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # *response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # *response.meta['item']['img'] = self.get_img(response)
        # *response.meta['item']['title'] = self.get_title(response)
        # *response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(SocialementresponsableSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def get_title(self, action):
        return self.extract_string_data_from_list(action, self.title_xpath, "title")

    def get_project_holder(self, action):
        return self.extract_string_data_from_list(action, self.project_holder_xpath, "project_holder")

    def get_contact(self, action):
        return self.extract_string_data_from_list(action, self.contact_xpath, "contact", joining=", ")

    def get_area(self, action):
        communes = self.extract_list(action, self.area_xpath, data_type="area")
        return { 'communes': communes }

    def get_next_page(self, response):
        """tries to find a new page to scrap.
        if it finds one, returns it along with a True value"""
        next_page = response.xpath(self.next_page_xpath).extract_first()
        if (next_page is not None) and (self.page_count < self.LIMIT):
            self.page_count += 1
            next_page = "/annuaire{}".format(next_page.strip())
            next_page = self.add_string_to_complete_url_if_needed(next_page, self.page_url)
            return True, next_page
        else:
            return False, next_page