import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action

import urllib

class LelaboessSpider(scrapy.Spider, CgetSpider):
    """Le Labo de l’ESS

Utilise scrapy-splash: nécessite une instance de splash

La région est présente sur les filtres mais pas dans les fiches, donc non récupérable.
"""
    LIMIT = 20
    name = "lelaboess"
    label = "Le Labo de l’ESS"
    page_url = "http://www.lelabo-ess.org"
    start_urls = ['http://www.lelabo-ess.org/-initiatives-inspirantes-.html', ]

    action_list_xpath = '//div[@class="menu menu_articles"]/ul/li'
    next_page_xpath = '//span[@class="pages"]/strong/following-sibling::a/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = './a/@href'
    abstract_xpath = './/small/text()'
    title_xpath = './/strong/text()'
    date_xpath = '//div[@id="content"]//abbr[@class="published"]/text()'

    # In action page
    area_xpath = ''
    key_words_xpath = '//div[@id="content"]//div[@class="tag"]/a/text()'
    contact_xpath = '//div[@id="content"]//p[contains(text(), "En savoir plus")]//a[@class="spip_out"]/@href'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    ajax_env = None

    def start_requests(self):
        """Request the urls using Splash to interprete the javascript too"""
        for url in self.start_urls:
            yield scrapy.Request(url, self.parse)

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        if self.ajax_env is None:
            ajax_env_not_formated = response.xpath('//div[@class="ajaxbloc"]/@data-ajax-env').extract_first()
            self.ajax_env = urllib.parse.quote(ajax_env_not_formated, safe='')

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse, headers={'Cookie':'mediaplan=R3757232358'})

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # *response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # *response.meta['item']['img'] = self.get_img(response)
        # *response.meta['item']['title'] = self.get_title(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(LelaboessSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def get_key_words(self, action):
        return self.extract_list_by_xpath(action, self.key_words_xpath, "key_words")

    def get_contact(self, action):
        return self.get_link(action, self.contact_xpath, "contact")

    def get_next_page(self, response):
        """tries to find a new page to scrap.
        if it finds one, returns it along with a True value"""
        next_page = response.xpath(self.next_page_xpath).extract_first()
        if (next_page is not None) and (self.page_count < self.LIMIT):
            self.page_count += 1
            next_page = next_page.strip()
            next_page = self.add_string_to_complete_url_if_needed(next_page, self.page_url)

            next_page = next_page.split("#")[0]

            url_params = "&var_ajax=1&var_ajax_env={}".format(self.ajax_env)
            url_params = "{}&var_ajax_ancre=pagination_articles2&var_t=1508935871281".format(url_params)

            next_page = "{}{}".format(next_page, url_params)

            return True, next_page
        else:
            return False, next_page

    def get_ajax_env(self, response):
        return urllib.parse.quote(self.get_data(response, self.ajax_env_xpath, data_type="ajax_env"), safe='')
