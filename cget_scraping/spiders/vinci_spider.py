import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class VinciSpider(scrapy.Spider, CgetSpider):
    """Fondation Vinci

Une seule page de résultats, pas besoin de "next_page"
"""
    LIMIT = 20
    name = "vinci"
    label = "Fondation Vinci"
    page_url = "https://www.fondation.veolia.com"
    start_urls = ['https://www.fondation-vinci.com/fondation/fr/recherche/projets.htm'
                  '&a=&r=&d=&t=Quartiers%20prioritaires%20et%20lien%20social&g=&p=&c=&q=#form',

                  'https://www.fondation-vinci.com/fondation/fr/recherche/projets.htm'
                  '&a=&r=&d=&t=Mobilit%C3%A9%20solidaire&g=&p=&c=&q=#form',

                  'https://www.fondation-vinci.com/fondation/fr/recherche/projets.htm'
                  '&a=&r=&d=&t=Acc%C3%A8s%20%C3%A0%20l%E2%80%99emploi&g=&p=&c=&q=#form',

                  'https://www.fondation-vinci.com/fondation/fr/recherche/projets.htm'
                  '&a=&r=&d=&t=Insertion%20par%20le%20logement&g=&p=&c=&q=#form',
                  ]
    list_css_selector = 'div.projet'
    next_page_xpath = '//a[@title="Aller à la page suivante"]/@href'

    img_xpath = './/img/@src'
    link_xpath = './/a/@href'
    abstract_xpath = './/div[@class="description"]/p/text()'
    title_xpath = './/h5/text()'
    date_xpath = './/div[@class="description"]/p[2]'
    area_xpath = './/div[@class="etiquette"]//text()'

    # In action page
    key_words_xpath = '//div[@class="chapeauText"]//em/text()'
    contact_xpath = '//div[@class="column column2interne"]/h3'
    video_xpath = ""
    state_xpath = ""
    project_holder_xpath = '//div[@class="column column2interne"]/h3'
    partner_xpath = '//div[@class="column column3 aplatgris floatRight"]//p[2]/strong[2]/text()'
    economic_xpath = ""

    def parse(self, response):
        for action in response.css(self.list_css_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(VinciSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def parse_detailed_page(self, response):
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        full_url = response.meta['item']['link']
        url_fields = full_url.split('/')
        last_part = url_fields[-1]
        title = last_part.split('.')[0]
        title = title.replace('_', ' ')
        response.meta['item']['title'] = title[0].upper() + title[1:]
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_contact(self, action):
        try:
            contact_final = []
            div_inside = action.xpath(self.project_holder_xpath)
            for div_small in div_inside:
                label_text = div_small.xpath('./text()').extract_first()
                if label_text == "Coordonnées de la structure ":
                    contact = div_small.xpath('./following-sibling::p//text()').extract()
                    for data in contact:
                        if data.strip() != "":
                            contact_final.append(data.strip())
                    return ", ".join(contact_final)
            return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "abstract item n <{}> " \
                    "page <{}>".format(e, self.item_count_depth_1, self.page_count)
            self.error_array.append(error)
            return None

    def get_project_holder(self, action):
        try:
            div_inside = action.xpath(self.project_holder_xpath)
            for div_small in div_inside:
                label_text = div_small.xpath('./text()').extract_first()
                if label_text == "Coordonnées de la structure ":
                    project_holder = div_small.xpath('./following-sibling::p/strong/text()').extract_first().strip()
                    return project_holder
            return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "abstract item n <{}> " \
                    "page <{}>".format(e, self.item_count_depth_1, self.page_count)
            self.error_array.append(error)
            return None

    def get_abstract(self, action):
        try:
            p_tag = action.xpath(self.abstract_xpath).extract_first().strip()
            if p_tag.startswith('Parrainé par'):
                return None
            else:
                return p_tag
        except AttributeError as e:
            print("\n\nERROR <{}>\n\n".format(e))
            return None

    def get_title(self, action):
        try:
            title = action.xpath(self.title_xpath).extract_first()
            if title is None:
                return action.xpath('.//h5/a/text()').extract_first()
            else:
                return title
        except AttributeError:
            return None

    def get_date(self, action):
        try:
            p_tag = action.xpath(self.date_xpath).extract_first().strip()
            return p_tag[len(p_tag) - 14:len(p_tag) - 4]
        except (AttributeError, IndexError):
            return None

    def get_partner(self, action):
        try:
            data = action.xpath(self.partner_xpath).extract_first()
            if data is not None:
                return "{} (Vinci)".format(data.strip())
            return data
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "partner", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_area(self, action):
        areas = self.extract_list(action, self.area_xpath, data_type="area")
        if len(areas) == 1:
            return {'regions': [areas[0]]}
        if len(areas) == 2:
            return {'regions': [areas[0]], 'communes': [areas[1]]}
