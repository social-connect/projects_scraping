import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action

class VeoliaSpider(scrapy.Spider, CgetSpider):
    """Fondation Veolia"""
    LIMIT = 76
    name = "veolia"
    label = "Fondation Veolia"
    page_url = "https://www.fondation.veolia.com"
    start_urls = ['https://www.fondation.veolia.com/fr/projets-soutenus', ]

    list_css_selector = 'div.block-doc-downloads'
    next_page_xpath = './following-sibling::li/a/@href'

    img_xpath = './/img/@src'
    link_xpath = './/div[@class="doc-item-press"]/a/@href'
    abstract_xpath = './/div[@class="doc-item-press"]/a/text()'
    title_xpath = './/div[@class="title-main"]/a/text()'
    date_xpath = './/span[@class="time"]/text()'

    # In action page
    area_xpath = '//ul[@class="program-list"]/li[1]/text()'
    key_words_xpath = '//div[@class="block-links-inner"]//h2[1]//text()'
    contact_xpath = ""
    video_xpath = ""
    state_xpath = ""
    project_holder_xpath = '//div[@class="block-links-inner"]//text()'
    partner_xpath = '//ul[@class="program-list"]/li[2]/text()'
    economic_xpath = ""

    def parse(self, response):
        for action in response.css(self.list_css_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            try:
                yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})
            except TypeError as e:
                error = "Error <{}> on item n <{}> (main search page count)" \
                        "page <{}>".format(e, self.item_count, self.page_count)
                self.error_array.append(error)

        current_page_item = response.css('li.pager-current')
        is_next_page, next_page = self.get_next_page(current_page_item)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(VeoliaSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def parse_detailed_page(self, response):
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        response.meta['item']['partner'] = self.get_partner(response)
        response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']

    def get_date(self, action):
        try:
            return action.xpath(self.date_xpath).extract_first().split("•")[1].strip()
        except (AttributeError, IndexError):
            return None

    def get_area(self, action):
        try:
            data = action.xpath(self.area_xpath).extract()
            if len(data):
                #import pdb;pdb.set_trace()
                place = "".join(data).strip()
                if place != "":
                    return {'not_filtered': place}
            return {}
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "area", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_partner(self, action):
        try:
            data = action.xpath(self.partner_xpath).extract()
            if data is not None:
                return "".join(data).strip()
            return data
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "partner", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_project_holder(self, action):
        try:
            separator = action.xpath('//div[@class="block-links-inner"]//h2[2]//text()').extract_first()
            if separator is not None:
                separator = separator.strip()
                list_block_to_filter = []
                datas = action.xpath(self.project_holder_xpath).extract()
                for data in datas:
                    if data.strip() != "":
                        list_block_to_filter.append(data.strip())
                block_to_filter = "".join(list_block_to_filter)
                project_holder = block_to_filter.split(separator)
                if len(project_holder)>1:
                    return project_holder[1]
                return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "project_holder", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_key_words(self, action):
        list_with_et_separator = self.extract_list(action, self.key_words_xpath, separator=" et ")
        if list_with_et_separator is not None:
            if not len(list_with_et_separator) > 1:
                return self.extract_list(action, self.key_words_xpath, separator=" & ")
        return list_with_et_separator
