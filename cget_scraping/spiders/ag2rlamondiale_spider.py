import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class Ag2rlamondialeSpider(scrapy.Spider, CgetSpider):
    """AG2R La Mondiale

Il n'y a pas de pagination, les projets sont tous sur la même page de recherche.
Il n'y a pas de lien par projet, le contact est le lien vers le site du projet lui-même."""
    LIMIT = 1
    name = "ag2rlamondiale"
    label = "AG2R La Mondiale"
    page_url = "http://www.fondation.ag2rlamondiale.fr"
    start_urls = ['http://www.fondation.ag2rlamondiale.fr/accueil/les-territoires.html', ]

    action_list_xpath = '//div[@class="ListeProjets"]/div/div'
    next_page_xpath = ''

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = ''
    abstract_xpath = './/div[@class="cutomizableRicheText"]//p[contains(./text(), "Région")]//following-sibling::p/text()'
    title_xpath = './div[1]//text()'
    date_xpath = ''
    area_xpath = './/div[@class="cutomizableRicheText"]//p[contains(./text(), "Région")]/text()'
    key_words_xpath = './/div[@class="cutomizableRicheText"]//p[contains(./text(), "Domaine")]/text()'
    contact_xpath = './/div[@class="cutomizableRicheText"]//a/@href'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            # url = item['link']
            # yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})
            yield item

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        # is_next_page, next_page = self.get_next_page(response)
        # if is_next_page:
        #     yield response.follow(next_page, callback=self.parse)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(Ag2rlamondialeSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def get_area(self, action):
        area_list = self.extract_list(action, self.area_xpath, data_type="area")
        area_list_final = []
        for area in area_list:
            area_list_final.append(area.split('Région : ')[1])
        return {'regions': area_list_final }

    def get_key_words(self, action):
        key_word_list = self.extract_list(action, self.key_words_xpath)
        key_word_list_final = []
        for key_word in key_word_list:
            key_word_list_final.append(key_word.split('Domaine : ')[1])
        return key_word_list_final

    def get_link(self, action, xpath=None, data_type="link"):
        if xpath is None:
            return "http://www.fondation.ag2rlamondiale.fr/accueil/les-territoires.html"
        try:
            link = action.xpath(xpath).extract_first()
            if link is not None:
                link = self.add_string_to_complete_url_if_needed(link, self.page_url)
            return link
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, data_type, self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_title(self, action):
        return self.extract_string_data_from_list(action, self.title_xpath, "title")
