import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class FondationCaritasFranceSpider(scrapy.Spider, CgetSpider):
    """Fondation Caritas

Les mot-clés contiennent aussi la localisation, mais impossible de le récupérer indépendamment."""
    LIMIT = 3
    name = "fondationcaritasfrance"
    label = "Fondation Caritas"
    page_url = "https://www.fondationcaritasfrance.org"
    start_urls = ['https://www.fondationcaritasfrance.org/nos-projets/tous-les-projets/', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//section[@id="list"]/div[@class="container"]/article'
    next_page_xpath = ''

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = '//img[@class="mainthumb"]/@src'
    link_xpath = './/div[contains(@class, "content-article")]/a/@href'
    abstract_xpath = ''
    title_xpath = './/h3/text()'
    date_xpath = ''
    key_words_xpath = './/ul[@class="taglist"]/li/a/text()'

    # In action page
    area_xpath = '(.//a[@rel="tag"])[1]/text()'
    contact_xpath = ''
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        # is_next_page, next_page = self.get_next_page(response)
        # if is_next_page:
        #    yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(FondationCaritasFranceSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


