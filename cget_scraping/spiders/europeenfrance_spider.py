import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class EuropeenfranceSpider(scrapy.Spider, CgetSpider):
    """Europe en France"""
    LIMIT = 106
    name = "europeenfrance"
    label = "Europe en France"
    page_url = "http://www.europe-en-france.gouv.fr"
    start_urls = ['http://www.europe-en-france.gouv.fr/Rendez-vous-compte/Projets-exemplaires?thematique=0&region=0&fonds=0&mot-cle=Mot%28s%29+cl%C3%A9%28s%29&annee=0&valider=Valider', ]

    action_list_xpath = '//div[contains(@class, "cam_header ")]'
    next_page_xpath = '//li[@class="next"]//a/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = './/div[@class="cam_btn1"]//a/@href'
    abstract_xpath = '//div[@id="text_l"]//div[@class="onlineEditor"]/p[1]//text()'
    title_xpath = './/p[@class="line_two"]//text()'
    date_xpath = './/p[@class="line_one"]//text()'

    # In action page
    area_xpath = './/p[@class="line_one"]//text()'
    key_words_xpath = './/p[@class="line_three"]//text()'
    contact_xpath = '//div[@id="t_three"]//a/@href'
    video_xpath = './/div[@class="cam_btn2"]//a/@href'
    state_xpath = ''
    project_holder_xpath = '//p[@class="line_three"]//text()'
    partner_xpath = './/p[@class="line_one"]//text()'
    economic_xpath = ''

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(EuropeenfranceSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def get_date(self, action):
        return self.extract_list_by_xpath(action, self.date_xpath, "date")[0]

    def get_area(self, action):
        area = self.extract_list_by_xpath(action, self.area_xpath, data_type="area")[3]
        if area is not None:
            area = area.split(':')[1].strip()
            return { 'regions' : [area, ] }
        return None

    def get_partner(self, action):
        return self.extract_list_by_xpath(action, self.partner_xpath, "partner")[1]

    def get_key_words(self, action):
        finnish_data = []
        key_words = self.get_data(action, self.key_words_xpath, "key_words")
        if key_words is not None:
            key_words = key_words.split(":")[1].split(";")
            for key_word in key_words:
                key_word = key_word.strip()
                if key_word != "":
                    finnish_data.append(key_word)
            return finnish_data
        return []

    def get_video(self, action):
        return self.get_link(action, self.video_xpath, "video")

    def get_project_holder(self, action):
        project_holder = self.extract_string_data_from_list(action, self.project_holder_xpath, "project_holder")
        if project_holder is not None:
            project_holder = project_holder.split("Bénéficiaire :")
            if len(project_holder) > 1:
                project_holder = project_holder[1].strip()
        return project_holder

    def get_abstract(self, action):
        return self.extract_string_data_from_list(action, self.abstract_xpath, "abstract")
