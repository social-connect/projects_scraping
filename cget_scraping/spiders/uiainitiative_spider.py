import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class UIAInitiativeSpider(scrapy.Spider, CgetSpider):
    """UIA Initiative"""
    LIMIT = 4
    name = "uiainitiative"
    label = "UIA Initiative"
    page_url = "http://www.uia-initiative.eu"
    start_urls = ['http://www.uia-initiative.eu/fr/search?keyword=&f%5B0%5D=content_type%3Aproject', 'http://www.uia-initiative.eu/fr/search?keyword=&f%5B0%5D=content_type%3Aproject&page=1']
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//div[@id="block-uia-content"]//div[@class="view-content"]/div[@class="views-row"]'
    # The following does not work. Maybe because the link to the next page is relative?
    # TODO: Investigate. For now, the only 2 result pages have been manually set in start_urls (see above).
    next_page_xpath = '//div[@id="block-uia-content"]//li[@class="pager__item pager__item--next"]/a/@href'

    # All the xpaths needed to fill the Action objects attributes
    link_xpath = './div[@class="views-field views-field-title"]/span/a/@href'
    title_xpath = './div[@class="views-field views-field-title"]/span/a/text()'

    # In action page
    economic_xpath = '//div[@class="taxonomy-term vocabulary-theme"]//div[contains(@class, "field--name-name field--type-string")]/text()'
    img_xpath = '//div[contains(@class, "field--name-field-visual")]/img/@src'
    partner_xpath = '//div[contains(@class, "field--name-title") and contains(text(), "Partnership")]/../div[contains(@class, "field field--name-field-description-wysiwyg")]/ul/li/text()'
    abstract_xpath = '//div[contains(@class, "field--name-title") and contains(text(), "Challenge addressed")]/../div[contains(@class, "field field--name-field-description-wysiwyg")]/descendant-or-self::*/text()'
    contact_xpath = '//div[contains(@class, "field--name-field-contact")]/div[@class="field__items"]//div[contains(@class, "field--name-field-contact-email")]/a/text()'

    # Not available
    date_xpath = ''
    key_words_xpath = ''
    project_holder_xpath = ''
    area_xpath = ''
    video_xpath = ''
    state_xpath = ''

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']

            # If a URL was found, go check it out.
            if url:
                yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        response.meta['item']['economic'] = self.get_economic(response)
        response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_partner(self, action):
        return self.extract_string_data_from_list(action, self.partner_xpath, 'partner', '<br>')

    def get_abstract(self, action):
        return self.extract_string_data_from_list(action, self.abstract_xpath, 'abstract', ' ')

    def get_contact(self, action):
        return self.extract_string_data_from_list(action, self.contact_xpath, 'contact', '<br>')

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(UIAInitiativeSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider
