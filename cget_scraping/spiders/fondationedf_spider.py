import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class FondationEDFSpider(scrapy.Spider, CgetSpider):
    """Fondation EDF"""
    LIMIT = 1
    name = "fondationedf"
    label = "Fondation EDF"
    page_url = "https://fondation.edf.com"
    start_urls = ['https://fondation.edf.com/fr/agir/carte-projets', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//div[@class="view-content"]//ul/li'
    next_page_xpath = ''

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = './article[@role="article"]/div[@class="node__content"]/h2/a/@href'
    title_xpath = './article[@role="article"]/div[@class="node__content"]/h2/a/span/text()'

    # In action page
    abstract_xpath = '//article[@role="article"]/div[@class="node__content"]/div[contains(@class, "field--name-body")]/div[@class="field__item"]/text()'
    economic_xpath = '//article[@role="article"]/div[@class="node__info"]/div[contains(@class, "field--name-field-project-odd")]/div[@class="field__item"]/text()'
    contact_xpath = '//article[@role="article"]/div[@class="association-info"]/div[@class="association-content"]/div[contains(@class, "field--name-field-association-website")]/div[@class="field__item"]/a/@href'
    area_xpath = '//article[@role="article"]/div[@class="node__info"]/div[contains(@class, "field--name-field-association-region")]/div[@class="field__item"]/text()'

    # Not available
    date_xpath = ''
    key_words_xpath = ''
    project_holder_xpath = ''
    video_xpath = ''
    state_xpath = ''
    partner_xpath = ''

    def parse(self, response):

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        # is_next_page, next_page = self.get_next_page(response)
        # if is_next_page:
        #     yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_area(self, action):
        try:
            areas = action.xpath(self.area_xpath).extract()
            return { 'regions': areas }
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "area", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(FondationEDFSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


