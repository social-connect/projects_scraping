import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class UrbactSpider(scrapy.Spider, CgetSpider):
    """Urbact

Pas de page suivante.
Deux xpath possibles pour les mot-clés : themes ou topics.
"""
    LIMIT = 3
    name = "urbact"
    label = "Urbact"
    page_url = "http://urbact.eu"
    start_urls = ['http://urbact.eu/all-networks', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//div[@class="view-content"]/div/article'
    next_page_xpath = ''

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = './/h2/a/@href'
    abstract_xpath = '//div[contains(@class, "field-name-field-introduction")]/div/div/p/text()'
    title_xpath = './/h2/a/div/div/div/text()'
    date_xpath = '(//div[contains(@class, "field-type-datetime")])[1]/div/div/span/@content'

    # In action page
    area_xpath = ''
    # key_words_xpath = '//main//div[contains(@class, "view-urbact-themes")]//span[@class="title"]/text()'
    key_words_xpath = '//div[contains(@class, "pane-urbact-topics-panel-pane-topics-sidebar")]//div[contains(@class, "views-row")]//a/text()'
    contact_xpath = ''
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = '//div[contains(@class, "lead-partner")]//div[contains(@class, "views-field-title")]/span/a/text()'
    economic_xpath = ''

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        # is_next_page, next_page = self.get_next_page(response)
        #if is_next_page:
         #   yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()
    
    def get_key_words(self, action):
        keywords = self.extract_list_by_xpath(action, self.key_words_xpath, data_type='key_words')
        if keywords:
            return keywords
        return []

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(UrbactSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


