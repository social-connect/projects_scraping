import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class AviseSpider(scrapy.Spider, CgetSpider):
    """Avise"""
    LIMIT = 20
    name = "avise"
    label = "Avise"
    page_url = "http://www.avise.org"
    start_urls = ['http://www.avise.org/portraits', ]

    list_xpath_selector = '//div[@class="view-content"]//div[@onclick]'
    next_page_xpath = '//li[@class="pager-next"]/a/@href'

    img_xpath = './/image/@*[name()="xlink:href"]'
    link_xpath = './/h2/a/@href'
    abstract_xpath = './/div[@class="field-item even"]/text()'
    title_xpath = './/h2/a/text()'

    # In action page
    date_xpath = '//div[@class="field field--name-field-created-year field--type-text field--label-inline ' \
                 'clearfix"]//div[@class="field-item even"]/text()'
    area_xpath = '//div[@class="addressfield-container-inline locality-block country-FR' \
                 '"]/span/text()'
    key_words_xpath = '//div[@class="field field--name-field-portrait-domain ' \
                      'field--type-text field--label-inline clearfix"]' \
                      '//div[@class="field-item even"]//text()'
    contact_xpath = '//div[@id="node-portrait-full-group-portrait-coordonnees"]//text()'
    video_xpath = ""
    state_xpath = ""
    project_holder_xpath = '//div[@id="node-portrait-full-group-portrait-coordonnees"]' \
                           '//div[@class="name-block"]/text()'
    partner_xpath = ""
    economic_xpath = ""


    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(AviseSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def parse(self, response):
        for action in response.xpath(self.list_xpath_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)


    def parse_detailed_page(self, response):
        self.item_count_depth_1 += 1
        # response.meta['item']['partner'] = self.get_partner(response)
        response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_area(self, action):
        try:
            areas = action.xpath(self.area_xpath).extract()
            return { 'communes': areas }
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "area", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_contact(self, action):
        try:
            datas = action.xpath(self.contact_xpath).extract()
            final_datas = []
            if len(datas)>0:
                for data in datas:
                    data = data.strip()
                    if data != "":
                        final_datas.append(data)
            return ", ".join(final_datas).strip()

        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "contact", self.item_count, self.page_count)
            self.error_array.append(error)
            return None
