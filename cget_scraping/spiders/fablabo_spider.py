import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class FablaboSpider(scrapy.Spider, CgetSpider):
    """Fablabo

N'est peut-être pas dans la liste des contributeurs officiels ?"""
    LIMIT = 3
    name = "fablabo"
    label = "Fablabo"
    page_url = "http://fablabo.net"
    start_urls = ['http://fablabo.net/mediawiki/index.php?title=Sp%C3%A9cial:Ask&offset=0&limit=30&q=[[Cat%C3%A9gorie%3AProjets]]&p=mainlabel%3D%2Fformat%3Dtemplate%2Flink%3Dnone%2Fheaders%3Dhide%2Ftemplate%3DListeVignettes&po=%3F%0A%3FDescription%0A%3FImage%0A&sort=Date+de+modification&order=descending', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//div[@id="mw-content-text"]/div[@class="vf-vignette"]'
    next_page_xpath = '//fieldset[@class="smw-ask-actions"]/a[contains(text(), "Suivant")]/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = './/a/@href'
    abstract_xpath = './/div[@class="vf-vignette-description"]/mark/text()'
    title_xpath = './/div[@class="vf-vignette-titre"]/mark/text()'
    date_xpath = ''

    # In action page
    area_xpath = ''
    key_words_xpath = ''
    contact_xpath = '//a[contains(@href, "/wiki/Utilisateur:")]/@href'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_contact(self, action):
        contact_data = self.get_data(action, self.contact_xpath, "contact")
        if contact_data is None:
            return ""
        return self.page_url + contact_data

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(FablaboSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


