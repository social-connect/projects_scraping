import scrapy
from scrapy_splash import SplashRequest
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action
import urllib.parse


class BrudedSpider(scrapy.Spider, CgetSpider):
    """Bruded

Nécessite scrapy-splash : pour lancer ce spider, une instance de splash est nécessaire.
Peu de données disponibles.
"""
    LIMIT = 20
    name = "bruded"
    label = "Bruded"
    page_url = "https://www.bruded.org/"
    start_urls = ['https://www.bruded.org/adherents-et-leurs-projets.html', ]

    ajax_env_xpath = '//div[@data-ajax-env]/@data-ajax-env'
    ajax_env = ""

    action_list_xpath = '//div[@id="recherches-container"]/a'
    next_page_xpath = '//div[@class="pagination block"]/a[@class="next"]/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = ''
    link_xpath = './@href'
    abstract_xpath = ''
    title_xpath = './/span[@class="titre_recherche_resultat"]/text()'
    date_xpath = ''

    # In action page
    area_xpath = ''
    key_words_xpath = ''
    contact_xpath = ''
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def start_requests(self):
        """Request the urls using Splash to interprete the javascript too"""
        for url in self.start_urls:
            yield SplashRequest(url, self.parse, args={'wait': 0.5})

    def parse(self, response):

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(BrudedSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


