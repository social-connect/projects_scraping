import json
import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class CaisseDepotTerritoiresSpider(scrapy.Spider, CgetSpider):
    """Territoires Conseil

Le site dispose d'une API pour récupérer les données, 
le spider l'utilise pour certains paramètres.
Pour les autres, voir `parse_detailed_page`.

Le contact n'est par récupéré complètement.
"""

    LIMIT = 10
    name = "caissedepotsterritoires"
    label = "Territoires Conseil"
    page_url = "http://www.caissedesdepotsdesterritoires.fr/"  # base url
    api_endpoint_url = "http://search.caissedesdepotsdesterritoires.fr/customermatrix/rest/engineSearchGuiEndPoint"
    api_params = "wt=json&spellcheck=true&queryPurpose=SearchResult&json.nl=map&_ps_relevancy=true&pql=sort(pertinence)view(BaseExperience)rows(600)start(0)"
    # start_urls = ['http://www.caissedesdepotsdesterritoires.fr/cs/ContentServer?pagename=Mairie-conseils/Page/ExperiencesDesTerritoires', ]
    start_urls = [ api_endpoint_url + '?' + api_params ]

    # Not used because retrieved results in json format
    action_list_xpath = '//div[@id="resultSet"]/article[@class="article"]'
    next_page_xpath = '//li[@class=" active"]/following-sibling::li/a'

    link_xpath = './/h2/a/@href'
    abstract_xpath = './/p[@class="chapo"]/text()'
    title_xpath = './/h2/a/text()'
    area_xpath = ''
    key_words_xpath = ''

    # In action page
    date_xpath = '//article[@class="article"]//i[@class="metadata-time"]/text()'
    img_xpath = '//article[@class="article"]/figure/img/@src'
    contact_xpath = '//article[@class="article"]/section[1]/h2/text()'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        url = self.api_endpoint_url + '?' + self.api_params
        jsonresponse = json.loads(response.body_as_unicode())

        for action in jsonresponse['response']['docs']:
            item = Action()
            item['title'] = action['title']
            item['link'] = action['url']
            item['abstract'] = action['assetChapo']
            item['key_words'] = action['keyLabel']
            item['video'] = []
            item['state'] = ''
            item['project_holder'] = ''
            item['img'] = ''
            item['date'] = ''
            item['contact'] = ''
            item['partner'] = []
            item['economic'] = ''
            item['area'] = { 'departements' : action['DepartementName'] }
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        response.meta['item']['img'] = self.get_img(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def format_next_page(self, next_page):
        before_ajax_env_attr = "&var_ajax=1&var_ajax_env="
        next_page = "{}{}{}".format(next_page, before_ajax_env_attr, self.ajax_env)
        return next_page
