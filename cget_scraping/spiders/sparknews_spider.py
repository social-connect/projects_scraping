import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class SparkNewsSpider(scrapy.Spider, CgetSpider):
    """Sparknews

La vidéo n'est pas récupérée alors qu'elle existe.
"""
    LIMIT = 24
    name = "sparknews"
    label = "Sparknews"
    page_url = "http://www.sparknews.com/"
    start_urls = ['http://www.sparknews.com/fr/videos/expert-selection', ]

    action_list_xpath = '//div[contains(./@class,"listes-videos")]/ul/li'
    next_page_xpath = '//ul[@class="pager"]/li[@class="pager-next"]/a'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/div[contains(./@class, "views-field-field-champ-media")]//img/@src'
    link_xpath = './/div[contains(./@class, "views-field-title")]//a/@href'
    date_xpath = ''

    # In action page
    title_xpath = '//h1[@id="page-title"]/text()'
    abstract_xpath = '//div[contains(./@class, "node-content")]//div[@class="body"]/text()'
    area_xpath = ''
    key_words_xpath = '//div[contains(./@class, "field-name-field-tagging")]//a/text()'
    contact_xpath = ''
    video_xpath = '//div[contains(./@class, "node-content")]//div[@class="unisubs-videoplayer"]/object/@data'
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(SparkNewsSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def get_key_words(self, action):
        return self.extract_list_by_xpath(action, self.key_words_xpath, "key words")
