import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class CommentFinancerMonProjetResponsableSpider(scrapy.Spider, CgetSpider):
    """Comment financer mon projet responsable

Peu d'information disponibles."""
    LIMIT = 4
    name = "commentfinancermonprojetresponsable"
    label = "Comment financer mon projet responsable"
    page_url = "http://commentfinancermonprojetresponsable.fr"
    start_urls = ['http://commentfinancermonprojetresponsable.fr/les-projets', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//ul[@id="myPortfolio"]/li/div[@class="border"]'
    next_page_xpath = '//nav[@class="pagination"]/span[@class="next"]/a/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = './/h2/a/@href'
    key_words_xpath = './/div[@class="article-tags"]/a/text()'

    # In action page
    title_xpath = '//h1/text()'
    contact_xpath = '//*[@id="project-show-link"]/a/@href'
    abstract_xpath = '//div[@class="container project-page"]/div[@class="row"][2]//p/descendant-or-self::*'

    # Not available
    economic_xpath = ''
    project_holder_xpath = ''
    date_xpath = ''
    area_xpath = ''
    video_xpath = ''
    state_xpath = ''
    partner_xpath = ''

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']

            # If a URL was found, go check it out.
            if url:
                yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_abstract(self, action):
        return self.extract_string_data_from_list(action, self.abstract_xpath, 'abstract', ' ')

    def get_key_words(self, action):
        keywords = self.extract_string_data_from_list(action, self.key_words_xpath, 'key_words', ' ')
        return keywords.replace('#', '').split(' ')


    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(CommentFinancerMonProjetResponsableSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


