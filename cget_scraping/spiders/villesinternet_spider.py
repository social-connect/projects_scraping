import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action
import json


class VillesInternetSpider(scrapy.Spider, CgetSpider):
    """ATLAAS

Ce spider utilise l'API disponible "/getAtlaasActions?z=n&p="
"""
    page_count = 0
    LIMIT = 18
    name = "villesinternet"
    label = "ATLAAS"
    page_url = "http://www.villes-internet.net"
    actions_url = "http://www.villes-internet.net/actions/"
    api_url = "http://www.villes-internet.net/getAtlaasActions?z=n&p="
    start_urls = [api_url + "1", ]

    action_list_xpath = ''
    next_page_xpath = ''

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = '//div[@class="logo thumbnail"]/@data-thumbnail'
    link_xpath = ''
    abstract_xpath = '//div[@name="synthesis"]//text()'
    title_xpath = ''
    date_xpath = '//div[@class="published"]/span[@class="date"][1]/text()'

    # In action page
    area_xpath = ''
    key_words_xpath = ''
    contact_xpath = '//a[@class="struct"]/@href'
    video_xpath = ''
    state_xpath = '//div[@class="status"]/span[@class="value"]/text()'
    project_holder_xpath = ''
    partner_xpath = '//div[@class="bloc contractors"]//div[@class="text editable"]//text()'
    economic_xpath = ''

    def parse(self, response):

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        jsonresponse = json.loads(response.body_as_unicode())

        if (len(jsonresponse['actions']) > 0) and (self.page_count < self.LIMIT):
            self.page_count += 1
            for action in jsonresponse['actions']:
                item = Action()
                item['title'] = action['title']
                item['link'] = self.actions_url + action['id']
                item['key_words'] = []
                for enjeu in action['enjeux']:
                    item['key_words'].append(enjeu['thematic_short'])
                item['project_holder'] = action['struct']['name']
                item['area'] = { 'communes': [action['struct']['name'], ] }
                url = item['link']

                yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

            # This part will check if there are other pages to crawl
            # and execute the parse method on them if found
            next_page = "{}{}".format(self.api_url, self.page_count + 1)
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        response.meta['item']['partner'] = self.get_partner(response)
        # *response.meta['item']['area'] = self.get_area(response)
        # *response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        response.meta['item']['video'] = self.get_video(response)
        response.meta['item']['state'] = self.get_state(response)
        # *response.meta['item']['project_holder'] = self.get_project_holder(response)
        response.meta['item']['economic'] = self.get_economic(response)
        response.meta['item']['img'] = self.get_img(response)
        # *response.meta['item']['title'] = self.get_title(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method.
        It should NOT be MysiteSpider"""
        spider = super(VillesInternetSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def get_abstract(self, action):
        return self.extract_string_data_from_list(action, self.abstract_xpath, "abstract")

    def get_contact(self, action):
        return self.get_link(action, self.contact_xpath, "contact")

    def get_video(self, action):
        return None

    def get_economic(self, action):
        return None
