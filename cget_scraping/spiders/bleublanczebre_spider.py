import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class BleuBlancZebreSpider(scrapy.Spider, CgetSpider):
    """Bleu Blanc Zebre

Les mot-clés contiennent également des informations sur la localisation, mais il n'est pas possible de récupérer la localisation de façon distincte."""

    LIMIT = 5
    name = "bleublanczebre"
    label = "Bleu Blanc Zebre"
    page_url = "http://bleublanczebre.fr"
    start_urls = ['http://bleublanczebre.fr/?s=', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//div[@id="left-area"]/article[contains(@class, "et_pb_post")]'
    next_page_xpath = '//div[contains(@class, "pagination")]/div[@class="alignleft"]/a/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = './/img/@src'
    link_xpath = './/h2/a/@href'
    abstract_xpath = '//div[@class="et_pb_text_inner"]/text()'
    title_xpath = './/h2/a/text()'
    date_xpath = './/span[@class="published"]/text()'
    key_words_xpath = './/a[@rel="category tag"]/text()'

    # In action page
    area_xpath = ''
    contact_xpath = '//div[contains(@class, "et_pb_column_1")]/div[contains(@class, "et_pb_button_module_wrapper")]/a/@href'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(BleuBlancZebreSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


