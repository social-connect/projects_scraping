import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class ReseaururalSpider(scrapy.Spider, CgetSpider):
    """Réseau rural

Nécessite Splash pour indexer les données.
"""
    LIMIT = 5
    page_count = 0
    name = "reseaurural"
    label = "Réseau rural"
    page_url = "http://www.reseaurural.fr"
    start_urls = ['http://www.reseaurural.fr/centre-de-ressources/recherche', ]

    list_css_selector = 'div#results > table.sticky-enabled tbody tr'
    next_page_xpath = '//li[@class="pager-next last"]/a/@href'
    img_xpath = 'img'
    link_xpath = './td[1]/a/@href'
    abstract_xpath = '//div[@class="inside"]//span[@class="label"]'
    title_xpath = './td[1]/a/text()'
    date_xpath = ''

    # New attributes
    area_xpath = './td[4]/text()'
    key_words_xpath = './td[5]/text()'
    contact_xpath = '//div[@class="inside"]//span[@class="label"]'
    video_xpath = ""
    state_xpath = ""
    project_holder_xpath = './td[2]/text()'
    partner_xpath = '//div[@class="inside"]//div'
    economic_xpath = ""

    def parse(self, response):
        for action in response.css(self.list_css_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(ReseaururalSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def parse_detailed_page(self, response):
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # Commented because this is getting a link to a page
        # where the contact can be found
        response.meta['item']['partner'] = self.get_partner(response)
        yield response.meta['item']

    def get_abstract(self, action):
        try:
            labels = action.xpath(self.abstract_xpath)
            for label in labels:
                label_text = label.xpath('./text()').extract_first()
                if label_text == "Résumé : ":
                    return "".join(label.xpath('./following-sibling::p[1]//text()').extract()).strip()
            return ''
        except AttributeError as e:
            error = "Error <{}> on " \
                    "abstract item n <{}> " \
                    "page <{}>".format(e, self.item_count_depth_1, self.page_count)
            self.error_array.append(error)
            return ''

    def get_contact(self, action):
        try:
            labels = action.xpath(self.contact_xpath)
            for label in labels:
                label_text = label.xpath('./text()').extract_first()
                if label_text == "Contact : ":
                    contact = label.xpath('./following-sibling::a[1]/@href').extract_first().strip()
                    return self.add_string_to_complete_url_if_needed(contact)
            return ''
        except AttributeError as e:
            error = "Error <{}> on " \
                    "abstract item n <{}> " \
                    "page <{}>".format(e, self.item_count_depth_1, self.page_count)
            self.error_array.append(error)
            return ''

    def get_partner(self, action):
        try:
            div_inside = action.xpath(self.partner_xpath)
            for div_small in div_inside:
                label_text = div_small.xpath('.//span[@class="label"]//text()').extract_first()
                if label_text == "Contributeur : ":
                    partner = "".join(div_small.xpath("./text()").extract())
                    if partner is not None:
                        return partner.strip()
            return ''
        except AttributeError as e:
            error = "Error <{}> on " \
                    "abstract item n <{}> " \
                    "page <{}>".format(e, self.item_count_depth_1, self.page_count)
            self.error_array.append(error)
            return ''

    def get_img(self, action):
        return self.add_string_to_complete_url_if_needed("/sites/all/themes/rrf/rrf/images/header.png")

    def get_area(self, action):
        area = self.extract_list(action, self.area_xpath, separator=" ")
        if area is not None:
            return {'not_filtered': area}
        return {}