import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class SemeozSpider(scrapy.Spider, CgetSpider):
    """Semeoz

La date est au format français.
On pourrait récupérer le texte complet plutôt que le résumé.
"""
    LIMIT = 3
    name = "semeoz"
    label = "Semeoz"
    page_url = "http://semeoz.info/"
    start_urls = ['http://semeoz.info/category/initiatives/', ]

    list_css_selector = 'div.post-container'
    next_page_xpath = '//a[@class="archive-nav-older fleft"]/@href'

    img_xpath = './/img/@src'
    link_xpath = './/h2[@class="post-title"]/a/@href'
    abstract_xpath = './/div[@class="post-excerpt"]//p/text()'
    title_xpath = './/h2[@class="post-title"]/a/text()'

    # In action page
    date_xpath = '//div[@class="post-meta-bottom"]//ul/li[@class="post-date"]/a/text()'
    area_xpath = ""
    key_words_xpath = '//div[@class="post-meta-bottom"]//ul/li[@class="post-tags"]/a/text()'
    contact_xpath = ""
    video_xpath = '//div[@class="post-content"]//iframe/@src'
    state_xpath = ""
    project_holder_xpath = ""
    partner_xpath = ""
    economic_xpath = ""

    def parse(self, response):
        for action in response.css(self.list_css_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(SemeozSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def parse_detailed_page(self, response):
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_key_words(self, action):
        return self.extract_list_by_xpath(action, self.key_words_xpath, "key words")
