import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class ReseauCollectivite53(scrapy.Spider, CgetSpider):
    """Réseau collectivités 53

Des filtres par domaines existent mais l'information n'est pas présente sur la fiche.
"""
    LIMIT = 3
    name = "reseaucollectivite53"
    label = "Réseau collectivités 53"
    page_url = "http://www.reseau-collectivites-53.fr/"
    start_urls = ['http://www.reseau-collectivites-53.fr/adherents-actions-collectivites-mayenne.php?what=actions&ville_adh=&nb_hab=&themes[]=&themes[]=&search=', ]

    action_list_xpath = '//div[@id="insidePage"]/h2/following-sibling::div'
    next_page_xpath = ''

    # All the xpaths needed to fill the Action objects attributes
    link_xpath = './a[@class="carre_suite"]/@href'
    title_xpath = './h3/text()'
    date_xpath = './h4/text()'

    # In action page
    area_xpath = '//div[@id="insidePage"]/h2[@class="preCartouche"]/text()'
    img_xpath = '//div[@id="insidePage"]/div[@class="actions_visuels"]/img/@src'
    abstract_xpath = '//div[@id="insidePage"]/div[@class="courant"]/p/text()'
    key_words_xpath = ''
    contact_xpath = ''
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = ''
    partner_xpath = ''
    economic_xpath = ''

    def parse(self, response):
        self.log("***Start scraping {}***".format(self.name))
        self.log("**-{}-**".format(response))

        # This part will retrieve data from the main research page and send
        # the Action object (item) to the detailed page to be completed
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        # is_next_page, next_page = self.get_next_page(response)
        # if is_next_page:
        #    yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        # response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    def get_date(self, action):
        try:
            data = action.xpath(self.date_xpath).extract_first()
            if data is not None:
                return data.split(' ')[-1]
            return data
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, data_type, self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_area(self, action):
        communes = self.extract_list(action, self.area_xpath, data_type="area")
        return { 'communes': communes }

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(ReseauCollectivite53, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider
