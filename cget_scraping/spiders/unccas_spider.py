"""Need to use scrapy-splash.
To run this spider, you need a splash instance running"""

import scrapy
from scrapy_splash import SplashRequest
from cget_scraping.mixins import CgetSpider
import urllib.parse
from cget_scraping.items import Action


class UnccasSpider(scrapy.Spider, CgetSpider):
    """UNCCAS"""
    LIMIT = 20
    name = "unccas"
    label = "UNCCAS"
    page_url = "http://www.unccas.org"
    start_urls = ['http://www.unccas.org/-banque-d-experiences-', ]

    ajax_env_xpath = '//div[@data-ajax-env]/@data-ajax-env'
    ajax_env = ""

    list_css_selector = 'ul.result-link-a-wp li a'
    next_page_xpath = '//li[@class=" active"]/following-sibling::li/a/@href'

    img_xpath = './/img/@src'
    link_xpath = './@href'
    abstract_xpath = './/div/p[@class="mt10"]/text()'
    title_xpath = './/div/h3[@class="h-like-t"]//text()'
    date_xpath = './/time[@itemprop="datePublished"]/@datetime'

    # In action page
    area_xpath = '//aside//h3[@itemprop="contentLocation"]/text()'
    key_words_xpath = '//a[@itemprop="keywords"]/text()'
    contact_xpath = '//aside//div[@id="collapse-1"]//text()'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = '//aside/ul/li/article/a[@data-target="#collapse-1"]//h3/text()'
    partner_xpath = '//aside//div[@id="collapse-4"]//text()'
    economic_xpath = ''

    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(url, self.parse, args={'wait': 0.5})

    def parse(self, response):
        for action in response.css(self.list_css_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield SplashRequest(url, callback=self.parse_detailed_page, meta={'item': item})

        if self.ajax_env == "":
            self.ajax_env = self.get_ajax_env(response)
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            next_page = self.format_next_page(next_page)
            yield SplashRequest(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        response.meta['item']['partner'] = self.get_partner(response)
        response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(UnccasSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def get_ajax_env(self, response):
        return urllib.parse.quote(self.get_data(response, self.ajax_env_xpath, data_type="ajax_env"), safe='')

    def format_next_page(self, next_page):
        next_page = next_page[0:len(next_page)-17]
        before_ajax_env_attr = "&var_ajax=1&var_ajax_env="
        next_page = "{}{}{}".format(next_page, before_ajax_env_attr, self.ajax_env)
        return next_page

    def get_title(self, action):
        try:
            data = action.xpath(self.title_xpath).extract()
            if data is not None:
                return "".join(data).strip()
            return data
        except AttributeError as e:
            self.error_array.append(e)
            return ""

    def get_project_holder(self, action):
        string_to_split = self.extract_string_data_from_list(action,
                                                             self.project_holder_xpath,
                                                             data_type="project_holder")
        if string_to_split:
            split_string = string_to_split.split("par :")
            if len(split_string) > 1:
                return split_string[1]
        return ""

    def get_contact(self, action):
        try:
            contact_no_email = self.extract_string_data_from_list(action, self.contact_xpath, "contact", joining=", ")
            if contact_no_email is None:
                return ""
            contact_no_email = contact_no_email.replace('\t', '').replace('\n', '')
            email_xpath = '//aside//div[@id="collapse-1"]//a/@href'
            email = self.get_data(action, email_xpath, "email")
            if email is None:
                return ""
            email = email.replace("mailto:", "")
            return contact_no_email.replace(", Cliquez ici", email)
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "contact", self.item_count, self.page_count)
            self.error_array.append(error)
            return ""

    def get_key_words(self, action):
        return self.extract_list_by_xpath(action, self.key_words_xpath, "key_words")

    def get_partner(self, action):
        return self.extract_string_data_from_list(action, self.partner_xpath, "partner", joining=" ")
