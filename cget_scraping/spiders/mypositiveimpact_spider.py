import scrapy

from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class MyPositiveImpactSpider(scrapy.Spider, CgetSpider):
    """My Positive Impact"""
    LIMIT = 4
    name = "mypositiveimpact"
    label = "My Positive Impact"
    page_url = "http://www.mypositiveimpact.org"
    start_urls = ['http://www.mypositiveimpact.org/les-solutions', ]
    # When using several start_urls, remember that the same page_count attribute will iterate for every page
    # and the Limit might be hit earlier than you could expect

    action_list_xpath = '//div[contains(@class, "Grid-gutters")]/div/article/div[@class="SolutionsItem_overflow"]'
    next_page_xpath = '//a[contains(@class, "Paging_next")]/@href'

    # All the xpaths needed to fill the Action objects attributes
    img_xpath = 'substring-before(substring-after(./a/@style, "url(\'"), "\')")'
    link_xpath = './a/@href'
    abstract_xpath = './div[@class="SolutionsItem_content"]/p/text()'
    title_xpath = './div[@class="SolutionsItem_content"]/a/text()'
    key_words_xpath = './/span[@class="Button Button-underline"]/text()'
    project_holder_xpath = './div[@class="SolutionsItem_content"]/h5/text()'
    economic_xpath = './div[@class="SolutionsItem_content"]/span[contains(@class, "Category")]/text()'

    # In action page
    contact_xpath = '//section[contains(@class, "SolutionEntry_contentWrapper")]//div[@class="CustomContent CustomContent-entry"]//a[contains(text(), "Site internet")]/@href'

    # Not available
    date_xpath = ''
    area_xpath = ''
    video_xpath = ''
    state_xpath = ''
    partner_xpath = ''

    def parse(self, response):
        """
        Retrieve data from the main research page and send
        the Action object (item) to the detailed page to be completed
        """
        for action in response.xpath(self.action_list_xpath):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        # This part will check if there are other pages to crawl
        # and execute the parse method on them if found
        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # response.meta['item']['abstract'] = self.get_abstract(response)
        # response.meta['item']['partner'] = self.get_partner(response)
        # response.meta['item']['area'] = self.get_area(response)
        # response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        # response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # response.meta['item']['img'] = self.get_img(response)
        # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(MyPositiveImpactSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider


