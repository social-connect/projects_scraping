import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action


class AprilesSpider(scrapy.Spider, CgetSpider):
    """Apriles"""
    LIMIT = 77
    name = "apriles"
    label = "Apriles"
    page_url = "http://www.apriles.net/index.php"

    start_urls = ['http://www.apriles.net/index.php?option=com_sobi2&Itemid=95&limit=5&limitstart=0',]

    list_css_selector = 'table.sobi2Listing > tr'
    next_page_xpath = '//a[@title="Suivante"]/@href'

    img_xpath = './/span[@class="sobi2Listing_field_objectif"]//img/@src'
    link_xpath = './/span[@class="sobi2ItemTitle"]/a/@href'
    abstract_xpath = './/span[@class="sobi2Listing_field_objectif"]//text()'
    title_xpath = './/span[@class="sobi2ItemTitle"]/a/text()'

    # In action page
    area_xpath = '//span[@id="sobi2Details_field_region"]/text()'
    key_words_xpath = '//ul[@class="sobi2Listing_field_type_action"]/li/text()'
    contact_xpath = '//a[@class="porteur"]'
    video_xpath = ''
    state_xpath = ''
    project_holder_xpath = '//span[@id="sobi2Details_field_porteur_action"]/text()'
    partner_xpath = '//span[@id="sobi2Details_field_partenaires"]/text()'
    economic_xpath = ''

    def parse(self, response):
        for action in response.css(self.list_css_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            url = item['link']
            yield scrapy.Request(url, callback=self.parse_detailed_page, meta={'item': item})

        is_next_page, next_page = self.get_next_page(response)
        if is_next_page:
            yield response.follow(next_page, callback=self.parse)

    def get_next_page_bis(self, response, no_page_url):
        has_next_page = True
        has_not_next_page = False
        next_page = response.xpath(self.next_page_xpath.format(1)).extract_first().strip()
        if not next_page.isdigit():
            next_page = response.xpath(self.next_page_xpath.format(3)).extract_first()
            try:
                next_page = next_page.strip()
            except AttributeError:
                next_page = None

        if next_page is not None:  # and (self.page_count < self.LIMIT):
            self.page_count += 1
            next_page = "{}{}".format(no_page_url, next_page)
            return has_next_page, next_page
        else:
            return has_not_next_page, next_page

    def parse_detailed_page(self, response):
        """Uncomment the attributes that you will complete in the detailed page"""
        self.item_count_depth_1 += 1
        # # response.meta['item']['abstract'] = self.get_abstract(response)
        response.meta['item']['partner'] = self.get_partner(response)
        response.meta['item']['area'] = self.get_area(response)
        response.meta['item']['key_words'] = self.get_key_words(response)
        response.meta['item']['contact'] = self.get_contact(response)
        # response.meta['item']['video'] = self.get_video(response)
        # response.meta['item']['state'] = self.get_state(response)
        response.meta['item']['project_holder'] = self.get_project_holder(response)
        # response.meta['item']['economic'] = self.get_economic(response)
        # # response.meta['item']['img'] = self.get_img(response)
        # # response.meta['item']['title'] = self.get_title(response)
        # response.meta['item']['date'] = self.get_date(response)
        yield response.meta['item']
        self.print_error()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        """rename the class name to your spider name in this method
        It should NOT be MysiteSpider"""
        spider = super(AprilesSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def get_area(self, action):
        regions = self.extract_list_by_xpath(action, self.area_xpath, data_type="area")
        return {'regions': regions}

    def get_key_words(self, action):
        return self.extract_list_by_xpath(action, self.key_words_xpath, data_type="key_words")

    def get_project_holder(self, action):
        try:
            finnish_data = []
            datas = action.xpath(self.project_holder_xpath).extract()
            if datas is not None:
                for data in datas:
                    data = data.strip()
                    if data != "":
                        finnish_data.append(data)
                return "".join(finnish_data)
            else:
                return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "project_holder", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_partner(self, action):
        return self.extract_list_by_xpath(action, self.partner_xpath, "partner")

    def get_contact(self, action):
        try:
            data = action.xpath(self.contact_xpath)
            contact_link = data.xpath("./@href").extract_first()
            contact_name = data.xpath("./text()").extract_first()
            if contact_link is not None:
                contact_link = self.add_string_to_complete_url_if_needed(contact_link)
                return "{} : {}".format(contact_name.strip(), contact_link.strip())
            return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "contact", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_abstract(self, action):
        try:
            finnish_data = []
            datas = action.xpath(self.abstract_xpath).extract()
            if datas is not None:
                for data in datas:
                    data = data.strip()
                    if data != "":
                        finnish_data.append(data)
                return "".join(finnish_data)
            else:
                return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "abstract", self.item_count, self.page_count)
            self.error_array.append(error)
            return None
