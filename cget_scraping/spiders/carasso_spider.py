import scrapy
from cget_scraping.mixins import CgetSpider
from cget_scraping.items import Action

class CarassoSpider(scrapy.Spider, CgetSpider):
    """Fondation Daniel et Nina Carasso

Pas de fiche par projet. Des critères sont disponibles pour filter sur la région, mais ne sont pas présents sur la page."""
    LIMIT = 3
    name = "carasso"
    label = "Fondation Daniel et Nina Carasso"
    page_url = "http://www.fondationcarasso.org"
    start_urls = ['http://www.fondationcarasso.org/fr/projets-soutenus', ]

    list_css_selector = 'div.view-content tr.odd'
    abstract_xpath = '//tr[@class="even"][{}]//p//text()'
    title_xpath = './td[1]/text()'
    date_xpath = './td[3]/span/text()'
    img_xpath = 'img'
    response = ""

    # New attributes
    area_xpath = ""
    key_words_xpath = './td[2]/text()'
    contact_xpath = ""
    video_xpath = ""
    state_xpath = ""
    project_holder_xpath = ""
    partner_xpath = ""
    economic_xpath = ""

    def parse(self, response):
        self.response = response
        for action in response.css(self.list_css_selector):
            item = Action()
            self.fill_item_from_results_page(action, item)
            item['link'] = self.start_urls[0]
            yield item

        self.print_error()

    def get_link(self, action):
        return self.start_urls[0]

    def get_img(self, action):
        return 'http://www.fondationcarasso.org/sites/all/themes/basic/images/header_home_page.jpg'

    def get_abstract(self, action):
        try:
            xpath_p = self.abstract_xpath.format(self.item_count)
            abstract = "".join(self.response.xpath(xpath_p).extract())
            return abstract.strip()
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, "abstract", self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(CarassoSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider