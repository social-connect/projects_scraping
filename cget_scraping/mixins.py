# -*- coding: utf-8 -*-


class CgetSpider:
    """Mixin class for the spiders"""

    error_array = []
    item_count = 0  # will be incremented each time a new item is created
    item_count_depth_1 = 0  # will be incremented each time an item is completed in detailed page
    LIMIT = 5  # The number of pages where the spider will stop
    page_count = 1  # The number of pages already scraped
    name = ""  # The name of the spider to use when executing the spider
    download_delay = 0  # The delay in seconds between each request. some website will block too many requests
    page_url = ""  # base url (ex : "https://www.mysite.org")
    label = ""
    start_urls = []  # List of URLs that will be crawled by the parse method
    list_css_selector = ""
    next_page_xpath = ""

    # All the xpaths needed to fill the Action objects attributes

    img_xpath = ""
    link_xpath = ""
    abstract_xpath = ""
    title_xpath = ""
    date_xpath = ""

    # New attributes
    area_xpath = ""
    key_words_xpath = ""
    contact_xpath = ""
    video_xpath = ""
    state_xpath = ""
    project_holder_xpath = ""
    partner_xpath = ""
    economic_xpath = ""

    def fill_item_from_results_page(self, action, item):
        """Fil item from results page"""
        self.item_count += 1
        if self.img_xpath != "":
            item['img'] = self.get_img(action)
        else:
            item['img'] = ""
        if self.title_xpath != "":
            item['title'] = self.get_title(action)
        else:
            item['title'] = ""
        if self.abstract_xpath != "":
            item['abstract'] = self.get_abstract(action)
        else:
            item['abstract'] = None
        if self.link_xpath != "":
            item['link'] = self.get_link(action)
        else:
            item['link'] = ""
        if self.date_xpath != "":
            item['date'] = self.get_date(action)
        else:
            item['date'] = None
        if self.area_xpath != "":
            item['area'] = self.get_area(action)
        else:
            item['area'] = {}
        if self.key_words_xpath != "":
            item['key_words'] = self.get_key_words(action)
        else:
            item['key_words'] = []
        if self.contact_xpath != "":
            item['contact'] = self.get_contact(action)
        else:
            item['contact'] = ""
        if self.video_xpath != "":
            item['video'] = self.get_video(action)
        else:
            item['video'] = ""
        if self.state_xpath != "":
            item['state'] = self.get_state(action)
        else:
            item['state'] = ""
        if self.project_holder_xpath != "":
            item['project_holder'] = self.get_project_holder(action)
        else:
            item['project_holder'] = ""
        if self.partner_xpath != "":
            item['partner'] = self.get_partner(action)
        else:
            item['partner'] = ""
        if self.economic_xpath != "":
            item['economic'] = self.get_economic(action)
        else:
            item['economic'] = ""

    def get_next_page(self, response):
        """tries to find a new page to scrap.
        if it finds one, returns it along with a True value"""
        next_page = response.xpath(self.next_page_xpath).extract_first()
        if (next_page is not None) and (self.page_count < self.LIMIT):
            self.page_count += 1
            next_page = next_page.strip()
            next_page = self.add_string_to_complete_url_if_needed(next_page, self.page_url)
            return True, next_page
        else:
            return False, next_page

    def get_abstract(self, action):
        return self.get_data(action, self.abstract_xpath, "abstract")

    def get_img(self, action):
        return self.get_link(action, self.img_xpath, data_type="img")

    def get_title(self, action):
        return self.get_data(action, self.title_xpath, "title")

    def get_link(self, action, xpath=None, data_type="link"):
        """Used to retrieve links.
        If no xpath is given, will get the data for the Action's link attribute"""
        if xpath is None:
            xpath = self.link_xpath
        try:
            link = action.xpath(xpath).extract_first()
            if link is not None:
                link = self.add_string_to_complete_url_if_needed(link, self.page_url)
            return link
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, data_type, self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def get_date(self, action):
        return self.get_data(action, self.date_xpath, "date")

    def get_data(self, action, xpath, data_type):
        """Basic method that will return the data in the first tag found by the xpath"""
        try:
            data = action.xpath(xpath).extract_first()
            if data is not None:
                return data.strip()
            return data
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, data_type, self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def add_string_to_complete_url_if_needed(self, not_complete_url, rest_of_url=None):
        """adds the missing beggining part of an url with the '/' if needed"""
        if rest_of_url is None:
            rest_of_url = self.page_url
        if not not_complete_url.startswith("http"):
            if not not_complete_url.startswith("/"):
                not_complete_url = "{}{}".format("/", not_complete_url)
            not_complete_url = "{}{}".format(rest_of_url, not_complete_url)
        return not_complete_url

    def get_area(self, action):
        return self.extract_list(action, self.area_xpath, data_type="area")

    def get_key_words(self, action):
        keywords = self.extract_list(action, self.key_words_xpath)
        if keywords:
            return keywords
        return []

    def get_contact(self, action):
        return self.get_data(action, self.contact_xpath, "contact")

    def get_video(self, action):
        return self.get_data(action, self.video_xpath, "video")

    def get_state(self, action):
        return self.get_data(action, self.state_xpath, "state")

    def get_project_holder(self, action):
        return self.get_data(action, self.project_holder_xpath, "project_holder")

    def get_partner(self, action):
        return self.get_data(action, self.partner_xpath, "partner")

    def get_economic(self, action):
        return self.get_data(action, self.economic_xpath, "economic")

    def extract_list(self, action, xpath, separator=",", data_type="key_words"):
        """Will return a list of data that have been seperated by the given separator argument"""
        list_to_return = []
        try:
            data = action.xpath(xpath).extract_first()
            if data is not None:
                for item in data.split(separator):
                    item = item.strip()
                    if item != "":
                        list_to_return.append(item)
                return list_to_return
            return data
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, data_type, self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def extract_list_by_xpath(self, action, xpath, data_type):
        """Will return a list of data found by xpath (without being split by separator)"""
        try:
            finnish_data = []
            datas = action.xpath(xpath).extract()
            if datas is not None:
                for data in datas:
                    data = data.strip()
                    if data != "":
                        finnish_data.append(data)
                return finnish_data
            else:
                return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, data_type, self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def extract_string_data_from_list(self, action, xpath, data_type, joining=""):
        """Will return a list of data found by xpath (without being split by separator)"""
        try:
            finnish_data = []
            datas = action.xpath(xpath).extract()
            if datas is not None:
                for data in datas:
                    data = data.strip()
                    if data != "":
                        finnish_data.append(data)
                my_data = joining.join(finnish_data)
                if my_data == "":
                    return None
                else:
                    return my_data
            else:
                return None
        except AttributeError as e:
            error = "Error <{}> on " \
                    "{} item n <{}> " \
                    "page <{}>".format(e, data_type, self.item_count, self.page_count)
            self.error_array.append(error)
            return None

    def print_error(self):
        """Will print all the Errors in the error_array"""
        if len(self.error_array) > 0:
            self.logger.info("\nErrors : \n")
            for error in self.error_array:
                self.logger.info("{}\n".format(error))

    def spider_closed(self, spider):
        """Send some status to logging"""
        self.logger.info("**** Spider closed: {} ****".format(spider.name))
        self.logger.info("--- {} Items retrieved (main search page count)".format(self.item_count))
        self.logger.info("--- {} Items retrieved (detailed page count)".format(self.item_count_depth_1))
        self.logger.info("--- {} Pages crawled".format(self.page_count))
        self.print_error()
        self.logger.info("***End scraping***\n")
