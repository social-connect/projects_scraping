import json
import logging
import requests
from logging.handlers import RotatingFileHandler

logger = logging.getLogger(__name__)


class GeoCodingGetter(object):
    """Geocoding getter"""
    localisation_count = 0
    project_count = 0

    # Directory of .json data from scrapping
    data_dir = "data"

    # Directory where put .geojson after process
    geojson_dir = "geojson"

    # Directory off geographic ressources (region, departement, collectivites locales)
    ressource_geo_dir = "cget_scraping/geo_loc/ressources_geo"

    # Geo ressource files
    ressource_geo = {}
    ressource_name = {
        'region': 'region.json',
        'departement': 'departement.json',
        'collectivite_locale': 'collectivite_locale.json'
    }

    def __init__(self):
        # loading geo ressource file
        for name, file_name in self.ressource_name.items():
            with open(self.ressource_geo_dir + "/" + file_name) as geo_file:
                raw_json = geo_file.read()
            self.ressource_geo[name] = json.loads(raw_json)

    def get_geolocalisation(self, spider_name, title, area):
        # Looking for area field
        if area is None:
            logger.debug("{}/{} : no area field".format(spider_name, title))
        else:
            addr = ''
            # extract info from area field
            if isinstance(area, str):
                addr = area
            elif 'communes' in area:
                for com in area['communes']:
                    addr = com + " "
            elif 'not_filtered' in area:
                addr = area['not_filtered'][-1]
            elif 'collectivites_locales' in area:
                searched_name = area['collectivites_locales'][0].lower().replace('-', ' ').strip()
                for collectivite, city in self.ressource_geo['collectivite_locale'].items():
                    if collectivite.count(searched_name):
                        addr = city
                        break
                if len(addr) == 0:
                    logger.debug("{}/{}: collectivite not found '{}'".format(spider_name, title, searched_name))
            elif 'departements' in area:
                name = area['departements'][0].lower().replace('-', ' ').strip()
                try:
                    addr = self.ressource_geo['departement'][name]
                except KeyError as error:
                    logger.debug("{}/{}: departement not found '{}'".format(spider_name, title, searched_name))
            elif 'regions' in area:
                name = area['regions'][0].lower().replace('-', ' ').strip()
                try:
                    addr = self.ressource_geo['region'][name]
                except KeyError as error:
                    logger.debug("{}/{}: region not found '{}'".format(spider_name, title, searched_name))
            else:
                logger.debug("{}/{}: no area field".format(spider_name, title))
                pass

            if addr == '': pass

            # asking for geolocation with api-adresse.data.gouv.fr
            request = u'https://api-adresse.data.gouv.fr/search/?q={}&limit=1'.format(addr)

            # parsing the result
            data = json.loads(requests.get(request).text)

            # adding the feature to the collection
            if 'features' in data and len(data['features']) >= 1:
                geo_loc_info = data['features'][0]
                return {'coordinates': geo_loc_info['geometry']['coordinates']}
            else:
                logger.debug("{}/{}: no result for '{}'".format(spider_name, title, addr))

            return {'coordinates': []}
