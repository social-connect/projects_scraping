# coding: utf8

import json

region = {}

with open("region") as region_file:
    region_ligne = region_file.readline()
    while len(region_ligne):
        raw_region=region_ligne.split(";")
        region_name = raw_region[0].strip().replace('-', ' ').lower()
        region_city = raw_region[1].strip().replace('-', ' ').lower()
        region[region_name]=region_city
        region_ligne = region_file.readline()

region_json = json.dumps(region, ensure_ascii=False)

with open("region.json", "w") as f:
    f.write(region_json)
