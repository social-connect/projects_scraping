# -*-coding:utf-8 -*

import json

with open("collectivites_locales") as f:
    l = f.readline()
    collectivite = {}
    while len(l):
        raw_collectivite=l.split(";")
        collectivite_name = raw_collectivite[1].strip().replace('-', ' ').lower()
        collectivite_city = raw_collectivite[0].split("-", 1)[1].strip().replace('-', ' ').lower()
        collectivite[collectivite_name]=collectivite_city
        l = f.readline()

collectivite_json = json.dumps(collectivite, ensure_ascii=False)

with open("collectivite_locale.json", "w") as f:
    f.write(collectivite_json)