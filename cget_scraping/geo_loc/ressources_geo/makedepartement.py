# -*-coding:utf-8 -*

import json

with open("departement") as f:
    l = f.readline()
    departement = {}
    while len(l):
        raw_departement=l.split(",")
        departement_name = raw_departement[1].strip().replace('-', ' ').lower()
        departement_city = raw_departement[2].strip().replace('-', ' ').lower()
        departement[departement_name] = departement_city
        l = f.readline()

departement_json = json.dumps(departement, ensure_ascii=False)

with open("departement.json", "w") as f:
    f.write(departement_json)