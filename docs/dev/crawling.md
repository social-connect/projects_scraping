# Indexation des sites

Chaque site indexé est défini dans un "spider", une classe `MonSiteSpider` qui contient la définition des "xpath" et la façon dont ils seront récupérés. Chaque spider est situé dans le module `cget_scraping.spiders`.

## Lancer l'indexation pour un spider manuellement

```
scrapy crawl <spider_name>
```

où `<spider_name>` est l'attribut `name` du module spider dans `cget_scraping/spiders/name_spider.py`.

Un pipeline `JsonExporter` pipeline redirige les données récupérées dans un fichier json, dans `data/spider_results` folder.

## Champs des spiders

Les champs suivants doivent être peuplés par le spider :

* **title** : [requis] the project name
* **img**:  url d'une image pour illustrer le projet
* **abstract**:  description du project
* **link** : [requis] url de la page indexée
* **date** : date du projet
* **area** : dictionnaire contenant un ou plusieurs champs parmi :
    * **regions**
    * **departments**
    * **communautes_communes**
    * **communes**
    * **not_filtered**
* **keywords** : liste des mot-clés
* **contact** : contact du projet
* **video** : liste des urls des vidéos éventuelles
* **state** : état du projet state
* **project_holder** : porteur du project
* **partner** : liste des partenaires du project
* **economic** : statut juridique du porteur du projet

Le pipeline remplit automatiquement les champs :

* **idx** : [requis] index, entier incrémental calculé depuis le spider
* **spider_name** : [requis] nom du spider
* **coordinates** : location [longitude, latitude] du projet

## Fichier modèle de spider (tempates spiders)


Deux modèles sont disponibles dans `cget_scraping/templates` (run `scrapy genspider -l` to list them):

* `cget` pour un site web simple
* `cgetsplash` pour les sites qui utilisent javascript pour afficher les données, et ont donc besoin de `scrapy_splash` pour les récupérer.

Pour créer un nouveau spider lancer par exemple :

```
scrapy genspider -t cget mysite mysite.org
```

ou `-t withsplash`

## Ajouter un spider

1. Generer le fichier python spider dans le répertoire `cget_scraping/spiders`

```
scrapy genspider -t cget mysite mysite.org
```

2. Ajouter l'import du spider dans le fichier `cget_scraping/spiders/__init__.py`

```
# cget_scraping/spiders/__init__.py
…
from .mysite_spider import MysiteSpider
…
```

3. Mettre à jour la variable `ALL_SPIDERS` dans `cget_scraping/spiders/__init__.py`
