# Installation

Installer les dépendances

## Avec `pip`

    pip install -r requirements.txt

## Avec `pipenv`

Si vous utilisez [pipenv](https://docs.pipenv.org/) un fichier `Pipfile` est disponible.

    pipenv install

*Plus d'information sur [install pipenv](https://docs.pipenv.org/install.html#installing-pipenv)*
