# Manipulate json files with `jq`

To produce the first item of each project with only title, area and link :

    jq '.[0] | {title, area, link}' *_projects.json | jq -s . > firstproject_area.json

Same with all items :

    jq '.[] | {title, area, link}' *_projects.json | jq -s . > allprojects_area.json

## Data from allprojects.json

List spiders in allprojects.json :

    jq '.[] | .spider_name' allprojects.json | uniq

Count spiders :

    jq 'unique_by(.spider_name) | length' allprojects.json
