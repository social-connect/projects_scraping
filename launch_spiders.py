# -*- coding: utf-8 -*-

import logging
from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
from cget_scraping.spiders import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--spider', '-s', nargs='+', required=False, help='Spiders classname')
parser.add_argument('--pipeline', '-p', required=False, help='Pipeline exporter {api|json}', default='api')
parser.add_argument('--full', '-f', nargs='?', required=False, help='Retrieve all pages')
parser.add_argument('--apiurl', nargs='?', required=False, help='Used with api pipeline. Override API base URL')
args = parser.parse_args()

s = get_project_settings()
# update setting to use the pipeline which write result in json file
s.update(dict(ITEM_PIPELINES={
    'cget_scraping.pipelines.RestExportPipeline': 300,
}))
if args.pipeline == 'json':
    s.update(dict(ITEM_PIPELINES={
        'cget_scraping.pipelines.JsonExportPipeline': 300,
    }))
if args.apiurl:
    s.update(dict(API_BASE_URL=args.apiurl))

configure_logging()
logger = logging.getLogger(__name__)

runner = CrawlerRunner(settings=s)


# launch spiders and store result in separated files
@defer.inlineCallbacks
def crawl():
    if args.spider:
        for spider in args.spider:
            if spider in str(ALL_SPIDERS):
                # convert to class object
                spider = globals()[spider]
                yield runner.crawl(spider)
            else:
                raise Exception('Unkwon spider', spider)
    else:
        for spider in DEMO_SPIDERS:
            yield runner.crawl(spider)
    reactor.stop()


crawl()
reactor.run()
