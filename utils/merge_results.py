# coding:utf-8

import json
import re
from os import listdir, renames, path


def merge_in_single_json_file(data_dir, result_path):
    # Processing data files
    # Finding .json files
    files_list = listdir(data_dir)
    files_list = [f for f in files_list if re.match(".*\.json$", f)]

    # for each .json we loop on projects
    all_projects = []
    for file_name in files_list:
        print("reading", file_name)
        decode_json = ""
        features = []

        # reading .json
        with open(data_dir + "/" + file_name) as  projects_file:
            decode_json = json.load(projects_file)
            for project in decode_json:
                all_projects.append(project)

    print("Number of projects :", len(all_projects))
    with open(result_path, 'w') as outfile:
        json.dump(all_projects, outfile)
