# Processing data files
# Finding .json files

import json
import re
from os import listdir, renames, path

from cget_scraping.spiders import *


def get_spiders_missing_xpath():
    """Get missing xpath in spiders"""
    spiders_missing_xpath = {
        'link': [],
        'keywords': [],
        'contact': [],
        'area': [],
        'date': [],
        'video': [],
    }
    for spider in DEMO_SPIDERS:
        if spider.link_xpath == '':
            spiders_missing_xpath['link'].append(spider)
        if spider.key_words_xpath == '':
            spiders_missing_xpath['keywords'].append(spider)
        if spider.contact_xpath == '':
            spiders_missing_xpath['contact'].append(spider)
        if spider.area_xpath == '':
            spiders_missing_xpath['area'].append(spider)
        if spider.date_xpath == '':
            spiders_missing_xpath['date'].append(spider)
        if spider.video_xpath == '':
            spiders_missing_xpath['video'].append(spider)

    return spiders_missing_xpath

def get_spiders():
    """List all spiders name"""
    return [(spider.name, spider.url, spider.label) for spider in ALL_SPIDERS]
